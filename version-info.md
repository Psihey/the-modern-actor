# `1.0.0` _(December 7th, 2016)_

- Support: Android 10+ ([Gingerbread MR1][Android-Gingerbread-MR1])

---

## Changes

First stable release.

## Dependencies

- [`com.android.support:animated-vector-drawable:24.1.1`][#com.android.support:*]
- [`com.android.support:appcompat-v7:24.1.1`][#com.android.support:*]
- [`com.android.support:cardview-v7:24.1.1`][#com.android.support:*]
- [`com.android.support:design:24.1.1`][#com.android.support:*]
- [`com.android.support:recyclerview-v7:24.1.1`][#com.android.support:*]
- [`com.android.support:support-annotations:24.1.1`][#com.android.support:*]
- [`com.android.support:support-v4:23.0.0`][#com.android.support:*]
- [`com.android.support:support-v4:24.1.1`][#com.android.support:*]
- [`com.android.support:support-vector-drawable:24.1.1`][#com.android.support:*]
- [`com.google.android.gms:play-services-ads-lite:9.4.0`][#com.google.android.gms:*]
- [`com.google.android.gms:play-services-ads:9.4.0`][#com.google.android.gms:*]
- [`com.google.android.gms:play-services-base:9.4.0`][#com.google.android.gms:*]
- [`com.google.android.gms:play-services-basement:9.4.0`][#com.google.android.gms:*]
- [`com.google.android.gms:play-services-clearcut:9.4.0`][#com.google.android.gms:*]
- [`com.google.android.gms:play-services-gass:9.4.0`][#com.google.android.gms:*]
- [`com.google.android.gms:play-services-tasks:9.4.0`][#com.google.android.gms:*]
- [`haibison.android:amateur-support-v4:25.+`][#haibison.android:amateur-support-v4]
- [`haibison.android:fad7:33.+`][#haibison.android:fad7]
- [`haibison.android:gi3:1.+`][#haibison.android:gi3]
- [`haibison.android:jrae:5.+`][#haibison.android:jrae]
- [`haibison.android:res:7.+`][#haibison.android:res]
- [`haibison.android:simple-provider:30.+`][#haibison.android:simple-provider]
- [`haibison.android:temp-file-provider:10.+`][#haibison.android:temp-file-provider]
- [`haibison.android:underdogs:3.+`][#haibison.android:underdogs]
- [`haibison.android:wake-lock-service:11.+`][#haibison.android:wake-lock-service]

---

[Android-Gingerbread-MR1]: http://developer.android.com/reference/android/os/Build.VERSION_CODES.html#GINGERBREAD_MR1

[#com.android.support:*]: https://developer.android.com/tools/support-library/index.html
[#com.google.android.gms:*]: https://developers.google.com/android/guides/setup
[#com.google.firebase:firebase-common]: https://firebase.google.com/docs/android/setup
[#haibison.android:amateur-support-v4]: https://bitbucket.org/haibison/amateur-support-v4
[#haibison.android:fad7]: https://bitbucket.org/haibison/fad7
[#haibison.android:gi3]: https://bitbucket.org/haibison/gi3
[#haibison.android:jrae]: https://bitbucket.org/haibison/jrae
[#haibison.android:res]: https://bitbucket.org/haibison/res
[#haibison.android:simple-provider]: https://bitbucket.org/haibison/simple-provider
[#haibison.android:temp-file-provider]: https://bitbucket.org/haibison/temp-file-provider
[#haibison.android:underdogs]: https://bitbucket.org/haibison/underdogs
[#haibison.android:wake-lock-service]: https://bitbucket.org/haibison/wake-lock-service