package app.widgets;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Build;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import app.R;

import haibison.android.fad7.utils.Views;

import static android.text.format.DateUtils.SECOND_IN_MILLIS;

import static app.BuildConfig.TAG;

/**
 * Media controller.
 */
public class MediaController extends LinearLayout {

    private static final String CLASSNAME = MediaController.class.getName();

    private static final long UPDATE_INTERVAL = SECOND_IN_MILLIS / 4;

    /**
     * Helper for {@link MediaPlayer}.
     */
    public interface MediaPlayerArrogantHelper {

        /**
         * Checks to see if the media player is playing or not.
         */
        boolean isPlaying();

    }//ArrogantHelper

    private ImageView imagePlayPause;
    private TextView textCurrentTime, textDuration;
    private SeekBar seekBar;

    private SimpleMediaPlayerEventListener mediaPlayerEventListener;
    private MediaPlayer mediaPlayer;

    private int imagePlayPauseBgrRes = R.drawable.selector__media_controller__button__play;

    private MediaPlayerArrogantHelper mediaPlayerArrogantHelper;
    private long mediaPlayerLastPosition, mediaPlayerLastPositionCheck;

    /**
     * <em>See parent's constructor for details about parameters.</em>
     */
    public MediaController(Context context) {
        super(context);
        initLayout();
    }//MediaController()

    /**
     * <em>See parent's constructor for details about parameters.</em>
     */
    public MediaController(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout();
    }//MediaController()

    /**
     * <em>See parent's constructor for details about parameters.</em>
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public MediaController(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initLayout();
    }//MediaController()

    /**
     * <em>See parent's constructor for details about parameters.</em>
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MediaController(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initLayout();
    }//MediaController()

    @Override
    protected void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);

        switch (visibility) {
        case VISIBLE: {
            if (mediaPlayer != null) post(mUpdater);
            break;
        }//VISIBLE

        default: {
            removeCallbacks(mUpdater);
            break;
        }//NOT VISIBLE
        }
    }//onWindowVisibilityChanged()

    @Override
    protected void onDetachedFromWindow() {
        removeCallbacks(mUpdater);

        super.onDetachedFromWindow();
    }//onDetachedFromWindow()

    /**
     * Initializes layout.
     */
    private final void initLayout() {
        setKeepScreenOn(true);
        setOrientation(HORIZONTAL);

        LayoutInflater.from(getContext()).inflate(R.layout.widget__media_controller, this, true);

        if (isInEditMode()) return;

        // MAP CONTROLS

        final View imagePlayPauseContainer = findViewById(R.id.widget__media_controller__img__play_pause__container);
        imagePlayPause = Views.findById(this, R.id.widget__media_controller__img__play_pause);
        textCurrentTime = Views.findById(this, R.id.widget__media_controller__text__current_time);
        textDuration = Views.findById(this, R.id.widget__media_controller__text__duration);
        seekBar = Views.findById(this, R.id.widget__media_controller__seek_bar);

        // SETUP CONTROLS

        imagePlayPauseContainer.setOnClickListener(mViewsOnClickListener);
        imagePlayPause.setEnabled(false);

        seekBar.setOnSeekBarChangeListener(seekBarOnChangeListener);
    }//initLayout()

    /**
     * Sets media player. You should set source and prepare it first.
     *
     * @param mediaPlayer the media player.
     */
    public void setMediaPlayer(final MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;

        imagePlayPause.setEnabled(mediaPlayer != null);
        seekBar.setEnabled(mediaPlayer != null);
        textCurrentTime.setText(DateUtils.formatElapsedTime(0));
        textDuration.setText(DateUtils.formatElapsedTime(0));

        if (mediaPlayer == null) return;

        final int durationInSeconds = (int) Math.max(0, mediaPlayer.getDuration() / SECOND_IN_MILLIS);
        seekBar.setMax(durationInSeconds);
        textDuration.setText(DateUtils.formatElapsedTime(durationInSeconds));

        if (mediaPlayerEventListener == null) mediaPlayerEventListener = new SimpleMediaPlayerEventListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                super.onCompletion(mp);

                imagePlayPause.setImageResource(R.drawable.selector__media_controller__button__play);
                textCurrentTime.setText(DateUtils.formatElapsedTime(seekBar.getMax()));
                seekBar.setProgress(seekBar.getMax());
            }//onCompletion()

            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                imagePlayPause.setImageResource(R.drawable.selector__media_controller__button__play);
                return true;
            }//onError()

        };

        mediaPlayer.setOnCompletionListener(mediaPlayerEventListener);
        mediaPlayer.setOnErrorListener(mediaPlayerEventListener);

        post(mUpdater);
    }//setMediaPlayer()

    /**
     * Sets a {@link MediaPlayerArrogantHelper}.
     *
     * @param arrogantHelper arrogant helper.
     */
    public void setMediaPlayerArrogantHelper(MediaPlayerArrogantHelper arrogantHelper) {
        mediaPlayerArrogantHelper = arrogantHelper;
    }//setMediaPlayerArrogantHelper()

    /**
     * Checks to see if media player is playing.
     *
     * @return {@code true} or {@code false}.
     */
    private boolean isMediaPlayerPlaying() {
        final MediaPlayer mp = mediaPlayer;
        if (mp == null) return false;

        try {
            final long currentPosition = mp.getCurrentPosition();
            try {
                if (mp.isPlaying()) return true;

                // Check last position
                if (mediaPlayerLastPosition != currentPosition && System.currentTimeMillis() - mediaPlayerLastPositionCheck <= SECOND_IN_MILLIS)
                    return true;

                // Ask for help from... arrogant helper
                final MediaPlayerArrogantHelper arrogantHelper = mediaPlayerArrogantHelper;
                return arrogantHelper != null ? arrogantHelper.isPlaying() : false;
            } finally {
                mediaPlayerLastPosition = currentPosition;
                mediaPlayerLastPositionCheck = System.currentTimeMillis();
            }
        } catch (Throwable t) {
            Log.e(TAG, t.getMessage(), t);
            return false;
        }
    }//isMediaPlayerPlaying()

    ////////////
    // LISTENERS
    ////////////

    /**
     * Click listener for all views.
     */
    private final View.OnClickListener mViewsOnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.widget__media_controller__img__play_pause__container: {
                if (mediaPlayer == null) break;

                try {
                    if (isMediaPlayerPlaying()) {
                        mediaPlayer.pause();
                        imagePlayPause.setImageResource(R.drawable.selector__media_controller__button__play);
                    } else {
                        mediaPlayer.start();
                        imagePlayPause.setImageResource(R.drawable.selector__media_controller__button__pause);
                    }
                } catch (Throwable t) {
                    Log.e(TAG, t.getMessage(), t);
                }

                break;
            }//PLAY/PAUSE CONTAINER
            }
        }//onClick()

    };// mViewsOnClickListener

    /**
     * Change listener for seek bar.
     */
    private final SeekBar.OnSeekBarChangeListener seekBarOnChangeListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            // Do nothing
        }//onStopTrackingTouch()

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            // Do nothing
        }//onStartTrackingTouch()

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser == false || mediaPlayer == null) return;

            try {
                mediaPlayer.seekTo((int) (progress * SECOND_IN_MILLIS));
                if (isMediaPlayerPlaying() == false) mediaPlayer.start();
            } catch (Throwable t) {
                Log.e(TAG, t.getMessage(), t);
            }
        }//onProgressChanged()

    };// seekBarOnChangeListener

    /**
     * UI updater.
     */
    private final Runnable mUpdater = new Runnable() {

        @Override
        public void run() {
            final MediaPlayer mediaPlayer = MediaController.this.mediaPlayer;

            boolean isPlaying = false;
            int currentPositionInSeconds = 0;

            if (mediaPlayer != null) try {
                isPlaying = isMediaPlayerPlaying();
                currentPositionInSeconds = (int) (mediaPlayer.getCurrentPosition() / SECOND_IN_MILLIS);
            } catch (Throwable t) {
                Log.e(TAG, t.getMessage(), t);
            }

            if (isPlaying) {
                if (imagePlayPauseBgrRes != R.drawable.selector__media_controller__button__pause)
                    imagePlayPause.setImageResource(imagePlayPauseBgrRes = R.drawable.selector__media_controller__button__pause);
            } else {
                if (imagePlayPauseBgrRes != R.drawable.selector__media_controller__button__play)
                    imagePlayPause.setImageResource(imagePlayPauseBgrRes = R.drawable.selector__media_controller__button__play);
            }

            textCurrentTime.setText(DateUtils.formatElapsedTime(currentPositionInSeconds));
            seekBar.setProgress(currentPositionInSeconds);

            postDelayed(this, UPDATE_INTERVAL);
        }//run()

    };// mUpdater

    /////////////////
    // HELPER CLASSES
    /////////////////

    /**
     * Simple event listener for media player. It does nothing for all events, so if you want to handle them, override the appropriate methods.
     */
    private static class SimpleMediaPlayerEventListener implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {

        @Override
        public void onCompletion(MediaPlayer mp) {
            // Do nothing
        }//onCompletion()

        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            // Do nothing
            return false;
        }//onError()

    }//SimpleMediaPlayerEventListener

}