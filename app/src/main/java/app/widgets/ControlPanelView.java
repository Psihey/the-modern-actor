package app.widgets;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import app.R;

/**
 * Control panel view.
 * <p/>
 * You can set a traditional click listener {@link View.OnClickListener}, and later verify which button was clicked via
 * {@link #getTouchedButton()}.
 */
public class ControlPanelView extends ImageView {

    /**
     * All buttons.
     */
    public static enum Button {
        WARM_UP(Color.RED),
        TONGUE_TWISTERS(Color.GREEN),
        ACTOR_VERBS(Color.BLUE),
        STORE(Color.CYAN),
        CENTER(Color.MAGENTA),
        UNKNOWN(Color.TRANSPARENT);

        /**
         * The color filter of this button.
         */
        private final int color;

        Button(int color) {
            this.color = color;
        }// Button()

        /**
         * Gets button from given color.
         *
         * @param color the button color.
         * @return the button.
         */
        public static Button getButton(int color) {
            for (final Button b : values()) if (b.color == color) return b;
            return UNKNOWN;
        }// getButton()

    }// Button

    private boolean touchedDown = false;
    private Button touchedButton = Button.UNKNOWN;
    private Bitmap background, bmpFilter;

    /**
     * <em>See parent's constructor for details.</em>.
     */
    public ControlPanelView(Context context) {
        super(context);
        init();
    }// ControlPanelView()

    /**
     * <em>See parent's constructor for details.</em>.
     */
    public ControlPanelView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }// ControlPanelView()

    /**
     * <em>See parent's constructor for details.</em>.
     */
    public ControlPanelView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }// ControlPanelView()

    /**
     * <em>See parent's constructor for details.</em>.
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ControlPanelView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }// ControlPanelView()

    /**
     * Initializes.
     */
    private final void init() {
        setScaleType(ScaleType.FIT_START);
        setImageResource(R.drawable.control_panel__bgr);

        background = ((BitmapDrawable) getDrawable()).getBitmap();
    }//init()

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        checkToInitBitmaps();
    }// onAttachedToWindow()

    /**
     * Checks to initialize all bitmaps.
     */
    private void checkToInitBitmaps() {
        if (bmpFilter == null || bmpFilter.isRecycled())
            bmpFilter = BitmapFactory.decodeResource(getResources(), R.drawable.control_panel__bgr__map);
    }//checkToInitBitmaps()

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        // RECYCLE BITMAPS

        if (bmpFilter != null) {
            bmpFilter.recycle();
            bmpFilter = null;
        }//if

        System.gc();
    }// onDetachedFromWindow()

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final boolean lastTouchedDown = touchedDown;
        boolean performClick = false;
        final Button lastTouchedButton = touchedButton;

        switch (MotionEventCompat.getActionMasked(event)) {
        case MotionEvent.ACTION_DOWN:
            touchedDown = true;
            break;

        case MotionEvent.ACTION_UP:
            touchedDown = false;
            performClick = true;
            break;

        case MotionEvent.ACTION_CANCEL:
            touchedDown = false;
            break;
        }

        if (touchedDown) {
            int x = (int) (event.getX() * bmpFilter.getWidth() / getWidth());
            if (x >= bmpFilter.getWidth()) x = bmpFilter.getWidth() - 1;
            if (x < 0) x = 0;

            int y = (int) event.getY() * bmpFilter.getHeight() / getHeight();
            if (y >= bmpFilter.getHeight()) y = bmpFilter.getHeight() - 1;
            if (y < 0) y = 0;

            touchedButton = Button.getButton(bmpFilter.getPixel(x, y));
        }// if

        if (lastTouchedButton != touchedButton || lastTouchedDown != touchedDown) {
            // Update background
            if (touchedDown) {
                switch (touchedButton) {
                case WARM_UP: setImageResource(R.drawable.control_panel__bgr__warm_up__selected); break;
                case TONGUE_TWISTERS: setImageResource(R.drawable.control_panel__bgr__tongue_twisters__selected); break;
                case ACTOR_VERBS: setImageResource(R.drawable.control_panel__bgr__actor_verbs__selected); break;
                case STORE: setImageResource(R.drawable.control_panel__bgr__store__selected); break;
                case CENTER: setImageResource(R.drawable.control_panel__bgr__center__selected); break;
                case UNKNOWN: setImageResource(R.drawable.control_panel__bgr); break;
                }
            } else {
                setImageResource(R.drawable.control_panel__bgr);
            }

            // Check to perform click
            if (performClick) performClick();

            return true;
        }// if

        return super.onTouchEvent(event);
    }// onTouchEvent()

    /**
     * Gets touched button.
     *
     * @return touched button.
     */

    public Button getTouchedButton() {
        return touchedButton;
    }// getTouchedButton()

}