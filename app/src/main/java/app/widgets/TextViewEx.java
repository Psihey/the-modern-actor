package app.widgets;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import app.R;

import haibison.android.underdogs.NonNull;

/**
 * Extended version of {@link TextView}, which supports setting asset typeface from XML.
 */
public class TextViewEx extends TextView {

    /**
     * @see TextView#TextView(Context)
     */
    public TextViewEx(Context context) {
        super(context);
        init(context, null);
    }//TextViewEx()

    /**
     * @see TextView#TextView(Context, AttributeSet)
     */
    public TextViewEx(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }//TextViewEx()

    /**
     * @see TextView#TextView(Context, AttributeSet, int)
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public TextViewEx(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }//TextViewEx()

    /**
     * @see TextView#TextView(Context, AttributeSet, int, int)
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TextViewEx(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }//TextViewEx()

    /**
     * Initializes.
     *
     * @param context the context.
     * @param attrs attribute set.
     */
    private final void init(@NonNull Context context, @Nullable AttributeSet attrs) {
        if (attrs != null) {
            final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TextViewEx);
            try {
                if (isInEditMode() == false) {
                    final String assetFont = a.getString(R.styleable.TextViewEx_assetFont);
                    if (TextUtils.isEmpty(assetFont) == false) setTypeface(Typeface.createFromAsset(getContext().getAssets(), assetFont));
                }//if
            } finally {
                a.recycle();
            }
        }//if
    }//init()

}