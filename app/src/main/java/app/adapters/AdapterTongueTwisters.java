package app.adapters;

import android.content.ContentProviderOperation;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import app.R;
import app.providers.TongueTwistersProvider;
import app.providers.TongueTwistersProvider.TongueTwisters;

import haibison.android.fad7.utils.Views;
import haibison.android.simpleprovider.SimpleContract;
import haibison.android.simpleprovider.services.CPOExecutor;
import haibison.android.simpleprovider.utils.CPOBuilder;

import static android.provider.BaseColumns._ID;

import static app.providers.TongueTwistersProvider.TongueTwisters.COLUMN_FAVORITE;
import static app.providers.TongueTwistersProvider.TongueTwisters.COLUMN_TITLE;
import static app.providers.TongueTwistersProvider.TongueTwisters.COLUMN_TONGUE_TWISTER;

import static haibison.android.simpleprovider.SimpleContract.FALSE_AS_INT;
import static haibison.android.simpleprovider.SimpleContract.TRUE_AS_INT;

/**
 * Adapter of Tongue Twisters.
 */
public class AdapterTongueTwisters extends RecyclerView.Adapter<AdapterTongueTwisters.ViewHolder> {

    /**
     * The view holder.
     */
    protected static class ViewHolder extends RecyclerView.ViewHolder {

        public final View content;
        public final TextView textTitle;
        public final CompoundButton checkBox;
        public final ImageView imageMoreCommands;

        /**
         * Creates new instance.
         *
         * @param itemView item view.
         */
        public ViewHolder(View itemView) {
            super(itemView);

            content = itemView.findViewById(R.id.list_item__tongue_twister__content);
            textTitle = Views.findById(itemView, R.id.list_item__tongue_twister__text__title);
            checkBox = Views.findById(itemView, R.id.list_item__tongue_twister__check_box);
            imageMoreCommands = Views.findById(itemView, R.id.list_item__tongue_twister__image__more_commands);
        }//ViewHolder()

    }//ViewHolder

    /**
     * Event listener.
     */
    public static interface EventListener {

        /**
         * Will be called when the user clicks an item.
         *
         * @param tongueTwisterId the tongue twister ID.
         */
        void onItemClick(long tongueTwisterId);

        /**
         * Will be called when the user clicks and holds (long-click) an item.
         *
         * @param tongueTwisterId the tongue twister ID.
         */
        void onItemLongClick(long tongueTwisterId);

    }// EventListener

    private final Context context;

    private Cursor cursor;
    private EventListener eventListener;

    /**
     * Creates new instance.
     *
     * @param context the context.
     */
    public AdapterTongueTwisters(final Context context) {
        this.context = context;
    }// AdapterTongueTwisters()

    @Override
    public int getItemCount() {
        return cursor == null ? 0 : cursor.getCount();
    }// getItemCount()

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (cursor.moveToPosition(position) == false) return;

        // LOAD DATA

        final long id = cursor.getLong(cursor.getColumnIndex(_ID));
        final int favorite = cursor.getInt(cursor.getColumnIndex(COLUMN_FAVORITE));
        String title = cursor.getString(cursor.getColumnIndex(COLUMN_TITLE));
        if (TextUtils.isEmpty(title)) title = cursor.getString(cursor.getColumnIndex(COLUMN_TONGUE_TWISTER));

        // FILL DATA TO CONTROLS

        holder.textTitle.setText(title);

        // SETUP LISTENERS

        holder.content.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (getEventListener() != null) getEventListener().onItemClick(id);
            }// onClick()

        });

        holder.content.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                if (getEventListener() != null) {
                    getEventListener().onItemLongClick(id);
                    return true;
                }// if

                return false;
            }// onLongClick()

        });

        // Set this listener to null first. Or we mess this up!!!
        holder.checkBox.setOnCheckedChangeListener(null);
        holder.checkBox.setChecked(favorite == TRUE_AS_INT);
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                final Uri contentUri = SimpleContract.getContentUri(context, TongueTwistersProvider.class, TongueTwisters.class);
                final ContentProviderOperation operation = CPOBuilder
                        .newUpdate(ContentUris.withAppendedId(contentUri, id))
                        .withValue(COLUMN_FAVORITE, isChecked ? TRUE_AS_INT : FALSE_AS_INT)
                        .build();
                CPOExecutor.IntentBuilder.newBatchOperations(context, null, contentUri.getAuthority()).addOperations(operation).start();
            }// onCheckedChanged()

        });

        // More commands
        holder.imageMoreCommands.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View view) {
                final PopupMenu menu = new PopupMenu(context, view);
                menu.inflate(R.menu.fragment__tongue_twisters__list_item);
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener () {

                    @Override
                    public boolean onMenuItemClick(final MenuItem item) {
                        switch (item.getItemId()) {
                        case R.id.action__delete: {
                            // Delete the record
                            final Uri itemUri = SimpleContract.getContentItemUri(
                                    context, TongueTwistersProvider.class, TongueTwisters.class, id
                            );
                            CPOExecutor.IntentBuilder.newBatchOperations(context, null, itemUri.getAuthority())
                                    .addOperations(CPOBuilder.newDelete(itemUri).build())
                                    .start();

                            break;
                        }//delete
                        }

                        return true;
                    }//onMenuItemClick()

                });

                menu.show();
            }//onClick()

        });
    }//onBindViewHolder()

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item__tongue_twister, parent, false);
        return new ViewHolder(itemView);
    }// onCreateViewHolder()

    /**
     * Changes cursor.
     *
     * @param cursor the cursor.
     */
    public void changeCursor(Cursor cursor) {
        if (this.cursor != null) this.cursor.close();
        this.cursor = cursor;
        notifyDataSetChanged();
    }// changeCursor()

    /**
     * Sets event listener.
     *
     * @param listener the listener.
     */
    public void setEventListener(EventListener listener) {
        eventListener = listener;
    }// setEventListener()

    /**
     * Sets event listener.
     *
     * @return the listener.
     */
    public EventListener getEventListener() {
        return eventListener;
    }// getEventListener()

}