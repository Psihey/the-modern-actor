package app.adapters;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import app.R;
import app.providers.WordsProvider.MainWords;
import app.providers.WordsProvider.SimilarWords;

import haibison.android.fad7.utils.Views;

import static android.provider.BaseColumns._ID;

/**
 * Adapter of Words (both Main and Similar).
 */
public class AdapterWords extends RecyclerView.Adapter<AdapterWords.ViewHolder> {

    /**
     * The view holder.
     */
    protected static class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView textWord;

        /**
         * Creates new instance.
         *
         * @param itemView item view.
         */
        public ViewHolder(View itemView) {
            super(itemView);

            textWord = Views.findById(itemView, R.id.text__word);
        }// ViewHolder()

    }// ViewHolder

    /**
     * Event listener.
     */
    public static interface EventListener {

        /**
         * Will be called when the user clicks an item.
         *
         * @param wordId     the word ID.
         * @param isMainWord {@code true} if this is main word, {@code false} if this
         *                   is similar word.
         */
        void onItemClick(long wordId, boolean isMainWord);

    }// EventListener

    private boolean usingMainWords = true;
    private Cursor cursor;
    private EventListener eventListener;

    /**
     * Creates new instance with main words mode.
     */
    public AdapterWords() {
    }// AdapterWords()

    @Override
    public int getItemCount() {
        return cursor == null ? 0 : cursor.getCount();
    }// getItemCount()

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (cursor.moveToPosition(position) == false) return;

        // LOAD DATA

        final long id = cursor.getLong(cursor.getColumnIndex(_ID));
        final String word = cursor.getString(
                cursor.getColumnIndex(usingMainWords ? MainWords.COLUMN_WORD : SimilarWords.COLUMN_WORD)
        );

        // FILL DATA TO CONTROLS

        holder.textWord.setText(word);

        // SETUP LISTENERS

        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (getEventListener() != null) getEventListener().onItemClick(id, usingMainWords);
            }// onClick()

        });
    }// onBindViewHolder()

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item__word, parent, false);
        return new ViewHolder(root);
    }// onCreateViewHolder()

    /**
     * Changes cursor.
     *
     * @param cursor         the cursor.
     * @param usingMainWords {@code true} for Main Words, {@code false} for Similar Words.
     */
    public synchronized void changeCursor(Cursor cursor, boolean usingMainWords) {
        usingMainWords = usingMainWords;

        if (this.cursor != null) this.cursor.close();
        this.cursor = cursor;

        notifyDataSetChanged();
    }// changeCursor()

    /**
     * Sets event listener.
     *
     * @param listener the listener.
     */
    public void setEventListener(EventListener listener) {
        eventListener = listener;
    }// setEventListener()

    /**
     * Sets event listener.
     *
     * @return the listener.
     */
    public EventListener getEventListener() {
        return eventListener;
    }// getEventListener()

}