package app.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import app.R;
import app.utils.Ads.AdMob;
import haibison.android.fad7.utils.Views;

/**
 * Base activity.
 * <p/>
 * This activity supports banner ads. To show banner ads, include this layout into yours: {@link app.R.layout#ad_view_container}. By default, it
 * uses {@link AdSize#SMART_BANNER}. That size is not so smart, so you can't use it in dialog mode in small screens. So you can override
 * {@link #getBannerAdSize()} and use {@link AdSize#BANNER} instead.
 */
public class BaseAdsActivity extends BaseActivity {

    /**
     * Makes new intent builder of {@link BaseAdsActivity}.
     *
     * @param context the context.
     * @return new intent builder.
     */
    @NonNull
    public static IntentBuilder newIntentBuilder(@NonNull Context context) {
        return new IntentBuilder(context, BaseAdsActivity.class);
    }//newIntentBuilder()

    private ViewGroup adViewContainer;
    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Ads
        setupBannerAds();
    }//onCreate()

    /**
     * Sets up banner ads. You should call this method after finishing setting up your content view.
     */
    protected void setupBannerAds() {
        if (AdMob.isSupported() == false || adViewContainer != null || adView != null) return;

        adViewContainer = Views.findById(this, R.id.ad_view_container);
        if (adViewContainer == null) return;

        adView = new AdView(this);
        adView.setAdSize(getBannerAdSize());
        adView.setAdUnitId(AdMob.BANNER_AD_UNIT_ID);

        adView.setAdListener(new AdListener() {

            @Override
            public void onAdFailedToLoad(int errorCode) {
                adViewContainer.removeAllViews();
            }//onAdFailedToLoad()

            @Override
            public void onAdLoaded() {
                // Make sure to remove all children views before adding new one
                adViewContainer.removeAllViews();
                adViewContainer.addView(adView);
            }//onAdLoaded()

        });

        adView.loadAd(AdMob.newAdRequest());
    }//setupBannerAds()

    /**
     * Gets banner ad size.
     *
     * @return banner ad size. Default is {@link AdSize#SMART_BANNER}.
     */
    protected AdSize getBannerAdSize() {
        return AdSize.SMART_BANNER;
    }//getBannerAdSize()

}