package app.activities;

import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.support.v4.app.Fragment;

import app.R;
import app.fragments.warmUp.FragmentAudioResourcePlayer;
import app.fragments.home.FragmentHome;
import app.fragments.home.FragmentHomeMenu;
import app.fragments.tongueTwister.FragmentTongueTwisters;
import app.fragments.actorVerbs.FragmentWords;
import app.utils.Intents;
import app.widgets.ControlPanelView;
import app.widgets.RightSlidingPaneLayout;

import haibison.android.fad7.Fad7;
import haibison.android.fad7.Fad7Handler;
import haibison.android.fad7.utils.Views;
import haibison.android.tfp.TempFileCleanerService;
import haibison.android.underdogs.NonNull;

import static app.utils.Constants.URL_GOOGLE_PLAY_STORE_DEV_PAGE;
import static app.utils.Constants.URL_HOMEPAGE;

/**
 * Main activity.
 */
public class MainActivity extends BaseAdsActivity {

    private static final int TASK_ID_FRAGMENT_HOME = 0;

    private RightSlidingPaneLayout rightSlidingPane;

    @Override
    protected boolean useToolbarAsActionBar() {
        return false;
    }//useToolbarAsActionBar()

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final Bundle fragmentHomeArgs = new Bundle();
        Fad7.addMessengerProviderAsParentActivity(fragmentHomeArgs);
        fragmentHomeArgs.putInt(Fad7.EXTRA_TASK_ID, TASK_ID_FRAGMENT_HOME);

        assignIntentBuilder()
                .setLayoutId(R.layout.activity__main)
                .addFragment(FragmentHome.class, R.id.fragment__home, fragmentHomeArgs)
                .addFragment(FragmentHomeMenu.class, R.id.fragment__home_menu);

        super.onCreate(savedInstanceState);

        // MAP CONTROLS

        rightSlidingPane = Views.findById(this, R.id.right_sliding_pane);

        // Clean up temp files
        TempFileCleanerService.IntentBuilder.newCleaner(this).start();
    }//onCreate()

    @Override
    public void onBackPressed() {
        // Close right sliding pane if it's available and is open
        if (rightSlidingPane != null && rightSlidingPane.isSlideable() && rightSlidingPane.isOpen()) {
            rightSlidingPane.closePane();
            return;
        }//if

        super.onBackPressed();
    }//onBackPressed()

    /////////////////////////////
    // MessengerProvider for Fad7
    /////////////////////////////

    @Override
    public Messenger getMessenger(@NonNull Fad7 fad7) {
        switch (fad7.getTaskId()) {
        case TASK_ID_FRAGMENT_HOME: return fad7Messenger;
        }

        return super.getMessenger(fad7);
    }//getMessenger()

    /**
     * Message handler for {@link Fad7}.
     */
    private final Messenger fad7Messenger = new Messenger(new Fad7Handler() {

        @Override
        public void handleMessage(@NonNull final Fad7 fad7, final int taskId, @NonNull final Message msg) {
            super.handleMessage(fad7, taskId, msg);

            switch (taskId) {
            case TASK_ID_FRAGMENT_HOME: {
                switch (msg.what) {
                case FragmentHome.MSG_CMD_CLICKED: {
                    // Close the sliding pane if it's available and is open
                    if (rightSlidingPane != null && rightSlidingPane.isSlideable() && rightSlidingPane.isOpen()) {
                        rightSlidingPane.closePane();
                        break;
                    }//if

                    // Handle the command
                    switch (ControlPanelView.Button.values()[msg.arg1]) {
                    case WARM_UP: {
                        if (rightSlidingPane != null && rightSlidingPane.isSlideable()) {
                            rightSlidingPane.openPane();

                            final Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment__home_menu);
                            if (fragment instanceof FragmentHomeMenu) ((FragmentHomeMenu) fragment).changeToLastActionGroup();
                        } else {
                            final Bundle args = new Bundle();
                            args.putInt(FragmentAudioResourcePlayer.EXTRA_AUDIO_RESOURCE, R.raw.mp3__physical_warmup);

                            BaseAdsActivity.newIntentBuilder(getContext())
                                    .setPadding(BaseAdsActivity.Padding.ALL)
                                    .setFragment(FragmentAudioResourcePlayer.class, args)
                                    .setTitle(R.string.text__physical_warm_up)
                                    .start();
                        }

                        break;
                    }// WARM_UP

                    case TONGUE_TWISTERS: {
                        if (rightSlidingPane != null && rightSlidingPane.isSlideable()) {
                            rightSlidingPane.openPane();

                            final Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment__home_menu);
                            if (fragment instanceof FragmentHomeMenu) ((FragmentHomeMenu) fragment).changeToFirstActionGroup();
                        } else {
                            // Open screen of Tongue Twisters
                            BaseAdsActivity.newIntentBuilder(MainActivity.this)
                                    .setFragment(FragmentTongueTwisters.class)
                                    .setTitle(R.string.text__tongue_twisters)
                                    .start();
                        }
                        break;
                    }// TONGUE_TWISTERS

                    case ACTOR_VERBS: {
                        // Open screen of database of Words
                        BaseAdsActivity.newIntentBuilder(MainActivity.this)
                                .setPadding(BaseAdsActivity.Padding.ALL)
                                .setFragment(FragmentWords.class)
                                .setTitle(R.string.text__actor_verbs)
                                .start();
                        break;
                    }// ACTOR_VERBS

                    case STORE: {
                        Intents.sendIntentToOpenUrl(MainActivity.this, URL_GOOGLE_PLAY_STORE_DEV_PAGE);
                        break;
                    }// STORE

                    case CENTER: {
                        // Open home page via external Internet browser
                        Intents.sendIntentToOpenUrl(MainActivity.this, URL_HOMEPAGE);
                        break;
                    }// CENTER
                    }

                    break;
                }//MSG_CMD_CLICKED
                }
                break;
            }//TASK_ID_FRAGMENT_HOME
            }
        }//handleMessage()

    });//fad7Messenger

}