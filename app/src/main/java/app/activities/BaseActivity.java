package app.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import app.R;
import haibison.android.fad7.ActivityWithFragments;
import haibison.android.fad7.utils.Views;
import haibison.android.underdogs.Param;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static haibison.android.res.intents.ActivityBuilder.EXTRA_TITLE;

/**
 * Base activity.
 */
public class BaseActivity extends ActivityWithFragments {

    private static final String CLASSNAME = BaseActivity.class.getName();

    /**
     * Screen orientation.
     */
    @Param(type = Param.Type.INPUT, dataTypes = int.class)
    public static final String EXTRA_SCREEN_ORIENTATION = CLASSNAME + ".SCREEN_ORIENTATION";

    /**
     * Intent builder.
     */
    public static class IntentBuilder extends ActivityWithFragments.IntentBuilder {

        /**
         * Makes new instance from a source intent. This can be used in case you make new activity which extends {@link ActivityWithFragments}:
         * in {@link #onCreate(Bundle)}, you make this builder with your intent, then set your extras, and call the super method.
         *
         * @param context the context.
         * @param source  source intent, which will be used <em>directly</em>.
         */
        public IntentBuilder(@NonNull Context context, @NonNull Intent source) {
            super(context, source);
        }//IntentBuilder()

        /**
         * Makes new builder.
         *
         * @param context the context.
         * @param cls     class extending {@link ActivityWithFragments}. If {@code null}, {@link ActivityWithFragments} will be used.
         */
        public IntentBuilder(@NonNull Context context, @NonNull Class<? extends ActivityWithFragments> cls) {
            super(context, cls);
        }//IntentBuilder()

        /**
         * Makes new builder of {@link ActivityWithFragments}.
         *
         * @param context the context.
         */
        public IntentBuilder(@NonNull Context context) {
            super(context);
        }//IntentBuilder()

        /**
         * Sets {@link #EXTRA_SCREEN_ORIENTATION}.
         *
         * @param screenOrientation see {@link #EXTRA_SCREEN_ORIENTATION}.
         * @return this builder.
         */
        @NonNull
        public <T extends IntentBuilder> T setScreenOrientation(int screenOrientation) {
            getIntent().putExtra(EXTRA_SCREEN_ORIENTATION, screenOrientation);
            return (T) this;
        }//setScreenOrientation()

    }//IntentBuilder

    /**
     * Makes new intent builder of {@link BaseActivity}.
     *
     * @param context the context.
     * @return new intent builder.
     */
    @NonNull
    public static IntentBuilder newIntentBuilder(@NonNull Context context) {
        return new IntentBuilder(context, BaseActivity.class);
    }//newIntentBuilder()

    private TextView textTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (useToolbarAsActionBar()) assignIntentBuilder()
                .setUseToolbarAsActionBar()
                .setUseActionBarStyleForToolbar()
                .setHomeAsUp(true);

        super.onCreate(savedInstanceState);

        // By default we use portrait mode in manifest. So we only need to change to "user" mode if this is a tablet device.
        if (getIntent().hasExtra(EXTRA_SCREEN_ORIENTATION)) setRequestedOrientation(
                getIntent().getIntExtra(EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_USER)
        ); else if (getResources().getBoolean(R.bool.tablet_size)) setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);

        // Setup custom view for action bar
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Inflate custom view
            final View customView = LayoutInflater.from(actionBar.getThemedContext()).inflate(R.layout.activity__action_bar_custom_view, null);

            // Add the custom view to action bar
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setCustomView(customView, new ActionBar.LayoutParams(WRAP_CONTENT, MATCH_PARENT, Gravity.CENTER));
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.getCustomView().getWidth();

            // MAP CONTROLS

            textTitle = Views.findById(actionBar.getCustomView(), R.id.text__title);

            // Set title
            if (getIntent().hasExtra(EXTRA_TITLE)) {
                final Object title = getIntent().getExtras().get(EXTRA_TITLE);
                if (title instanceof Integer) textTitle.setText((Integer) title);
                else textTitle.setText((CharSequence) title);
            }//if
        }//if
    }// onCreate()

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);

        if (textTitle != null) textTitle.setText(title);
    }//setTitle()

    @Override
    public void setTitle(int titleId) {
        super.setTitle(titleId);

        if (textTitle != null) textTitle.setText(titleId);
    }//setTitle()

    /**
     * Will be called by this activity to check if it should use toolbar as action bar.
     *
     * @return {@code true} or {@code false}.
     */
    protected boolean useToolbarAsActionBar() {
        return true;
    }//useToolbarAsActionBar()

    /**
     * Gets text title. It's only available if {@link #useToolbarAsActionBar()} returns {@code true}.
     *
     * @return the text title.
     */
    @Nullable
    protected TextView getTextTitle() {
        return textTitle;
    }//getTextTitle()

}