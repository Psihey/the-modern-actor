package app.providers;

import android.support.annotation.Nullable;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;

import app.R;

import haibison.android.simpleprovider.SimpleProvider;
import haibison.android.simpleprovider.annotation.Column;
import haibison.android.simpleprovider.annotation.Table;
import haibison.android.simpleprovider.database.BaseTable;

import static app.BuildConfig.TAG;
import static app.utils.Configurations.IO_BUFFER;

/**
 * Words provider.
 */
public class WordsProvider extends SimpleProvider {

    private static final String CLASSNAME = WordsProvider.class.getName();

    /**
     * Default database file name.
     */
    public static final String DB_FILENAME = "words.sqlite";

    @Nullable
    @Override
    protected String getDatabaseFileName() {
        return DB_FILENAME;
    }//getDatabaseFileName()

    @Override
    protected void onPrepareDatabase() {
        super.onPrepareDatabase();

        final File dbFile = getContext().getDatabasePath(DB_FILENAME);
        if (dbFile.isFile() == false) {
            // Android just tell us that the file name should be this. But it does NOT make sure the parent directory exists or not.
            // So we make it.
            if (dbFile.getParentFile().isDirectory() == false) dbFile.getParentFile().mkdirs();

            // Copy default DB to internal database folder.
            final InputStream inputStream = new BufferedInputStream(getContext().getResources().openRawResource(R.raw.database__words));
            try {
                try {
                    OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(dbFile));
                    try {
                        byte[] buf = new byte[IO_BUFFER];
                        int read;
                        while ((read = inputStream.read(buf)) >= 0) outputStream.write(buf, 0, read);
                    } finally {
                        outputStream.close();
                    }
                } finally {
                    inputStream.close();
                }
            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }//
    }//onPrepareDatabase()

    @Table("main_words")
    public interface MainWords extends BaseTable {

        /**
         * The word.
         */
        @Column(type = Column.Type.TEXT, notNull = true)
        String COLUMN_WORD = "word";

    }//MainWords

    @Table("similar_words")
    public interface SimilarWords extends BaseTable {

        /**
         * The ID of main word.
         */
        @Column(
                type = Column.Type.INTEGER, notNull = true,
                foreignKey = true, foreignKeyReferenceTable = MainWords.class, foreignKeyReferenceColumn = _ID
        )
        String COLUMN_MAIN_WORD_ID = "main_word_id";

        /**
         * The word.
         */
        @Column(type = Column.Type.TEXT, notNull = true)
        String COLUMN_WORD = "word";

    }//SimilarWords

}