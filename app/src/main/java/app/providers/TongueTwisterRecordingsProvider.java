package app.providers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.File;

import haibison.android.simpleprovider.SimpleProvider;
import haibison.android.simpleprovider.annotation.Column;
import haibison.android.simpleprovider.annotation.Table;
import haibison.android.simpleprovider.database.BaseTimingTable;

import static haibison.android.simpleprovider.database.BaseTimingTable._DATE_CREATED;
import static haibison.android.simpleprovider.utils.SQLite.DESC;
import static haibison.android.simpleprovider.utils.Strings.WS;

/**
 * Tongue Twister Recordings Provider.
 */
public class TongueTwisterRecordingsProvider extends SimpleProvider {

    @Nullable
    @Override
    protected String getDatabaseFileName() {
        return "tongue_twister_recordings.sqlite";
    }//getDatabaseFileName()

    @Table(value = "tongue_twister_recordings", defaultSortOrder = _DATE_CREATED + WS + DESC)
    public interface TongueTwisterRecordings extends BaseTimingTable {

        /**
         * File path.
         * <p>
         * <h1>Notes</h1>
         * <p>
         * <ul>
         * <li>You have to manually delete this file if you're going to delete the corresponding row in database.</li>
         * </ul>
         */
        @Column(type = Column.Type.TEXT)
        String COLUMN_FILE_PATH = "file_path";

        /**
         * Duration, in milliseconds.
         */
        @Column(type = Column.Type.INTEGER)
        String COLUMN_DURATION = "duration";

    }//TongueTwisterRecordings

    /**
     * File extension.
     */
    public static final String FILE_EXT = ".mp4";

    /**
     * Gets directory of recording files.
     *
     * @return directory of recording files.
     */
    @NonNull
    public static File getRecordingsDirectory(@NonNull Context context) {
        return context.getDir("tongue-twister-recordings", Context.MODE_PRIVATE);
    }//getRecordingsDirectory()

}