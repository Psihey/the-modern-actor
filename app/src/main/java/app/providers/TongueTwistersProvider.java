package app.providers;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

import app.R;

import haibison.android.simpleprovider.SimpleProvider;
import haibison.android.simpleprovider.annotation.Column;
import haibison.android.simpleprovider.annotation.Table;
import haibison.android.simpleprovider.database.BaseTimingTable;

import static app.providers.TongueTwistersProvider.TongueTwisters.COLUMN_TONGUE_TWISTER;
import static app.providers.TongueTwistersProvider.TongueTwisters.COLUMN_TYPE;
import static app.providers.TongueTwistersProvider.TongueTwisters.TYPE_PREDEFINED;

import static haibison.android.simpleprovider.SimpleContract.FALSE_AS_INT;
import static haibison.android.simpleprovider.database.BaseTimingTable._DATE_MODIFIED;
import static haibison.android.simpleprovider.utils.SQLite.DESC;
import static haibison.android.simpleprovider.utils.Strings.WS;

/**
 * Tongue Twisters Provider.
 */
public class TongueTwistersProvider extends SimpleProvider {

    @Nullable
    @Override
    protected String getDatabaseFileName() {
        return "tongue_twisters.sqlite";
    }//getDatabaseFileName()

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return super.query(uri, projection, selection, selectionArgs, sortOrder);
    }

    @Override
    protected void onAfterDatabaseCreated(SQLiteDatabase db) {
        super.onAfterDatabaseCreated(db);

        // Insert pre-installed tongue twisters
        final String tableName = TongueTwisters.class.getAnnotation(Table.class).value();
        final String[] tongueTwisters = getContext().getResources().getStringArray(R.array.preinstalled_tongue_twisters);
        for (String tongueTwister : tongueTwisters) {
            final ContentValues values = new ContentValues();
            values.put(COLUMN_TYPE, TYPE_PREDEFINED);
            values.put(COLUMN_TONGUE_TWISTER, tongueTwister);
            db.insert(tableName, null, values);

        }// for

    }//onAfterDatabaseCreated()


    @Table(value = "tongue_twisters", defaultSortOrder = _DATE_MODIFIED + WS + DESC)
    public interface TongueTwisters extends BaseTimingTable {

        /**
         * Predefined.
         */
        int TYPE_PREDEFINED = 0;

        /**
         * User-defined.
         */
        int TYPE_USER_DEFINED = 1;

        /**
         * Type.
         */
        @Column(type = Column.Type.INTEGER, notNull = true, hasDefaultValue = true, defaultValueAsLong = TYPE_USER_DEFINED)
        String COLUMN_TYPE = "type";

        /**
         * The tongue twister.
         */
        @Column(type = Column.Type.TEXT, notNull = true)
        String COLUMN_TONGUE_TWISTER = "tongue_twister";

        /**
         * The title.
         */
        @Column(type = Column.Type.TEXT)
        String COLUMN_TITLE = "title";

        /**
         * Flag for favorite.
         */
        @Column(type = Column.Type.INTEGER, hasDefaultValue = true, defaultValueAsLong = FALSE_AS_INT)
        String COLUMN_FAVORITE = "favorite";

    }//TongueTwisters

}