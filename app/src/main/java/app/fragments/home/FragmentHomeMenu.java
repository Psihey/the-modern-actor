package app.fragments.home;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;

import app.activities.BaseAdsActivity;
import app.R;
import app.fragments.base.BaseFragment;
import app.fragments.warmUp.FragmentAudioResourcePlayer;
import app.fragments.tongueTwister.recording.FragmentTongueTwisterRecordings;
import app.fragments.tongueTwister.FragmentTongueTwisters;
import app.fragments.warmUp.FragmentVideoResourcePlayer;
import app.fragments.tongueTwister.create.TongueTwisterCreatorFragment;
import app.utils.Action;

import haibison.android.fad7.utils.Views;

/**
 * Fragment Home menu.
 */
public class FragmentHomeMenu extends BaseFragment {

    private static final String CLASSNAME = FragmentHomeMenu.class.getName();

    private static final String INTERNAL_EXTRA_ACTIONS = CLASSNAME + ".INTERNAL.ACTIONS";

    private RecyclerView recyclerView;
    private Action[][] arraysOfActions;
    private ActionAdapter actionAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View contentView = inflater.inflate(R.layout.fragment__home_menu, container, false);

        // MAP CONTROLS

        recyclerView = Views.findById(contentView, R.id.recycler);

        return contentView;
    }// onCreateView()

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Get actions
        arraysOfActions = Action.parseArraysOfActions(getContext(), R.array.fragment__home_menu__actions);
        final Action[] actions;
        if (getArguments().containsKey(INTERNAL_EXTRA_ACTIONS)) actions = getActionsFromArguments();
        else {
            actions = arraysOfActions[0];
            getArguments().putParcelableArray(INTERNAL_EXTRA_ACTIONS, actions);
        }

        // SETUP CONTROLS

        actionAdapter = new ActionAdapter(actions);
        actionAdapter.setHasStableIds(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(actionAdapter);

        final View cmdRefresh = view.findViewById(R.id.cmd_view__refresh);
        if (arraysOfActions.length > 1) {
            cmdRefresh.setVisibility(View.VISIBLE);
            cmdRefresh.setOnClickListener(buttonsOnClickListener);
        } else cmdRefresh.setVisibility(View.GONE);
    }//onViewCreated()

    /**
     * Gets action from arguments.
     *
     * @return the actions.
     */
    @Nullable
    private Action[] getActionsFromArguments() {
        final Parcelable[] parcelables = getArguments().getParcelableArray(INTERNAL_EXTRA_ACTIONS);
        if (parcelables == null) return null;

        final Action[] actions = new Action[parcelables.length];
        for (int i = 0; i < parcelables.length; i++) actions[i] = (Action) parcelables[i];
        return actions;
    }//getActionsFromArguments()

    /**
     * Changes to next action group.
     */
    private void changeToNextActionGroup() {
        final Action[] actions = getActionsFromArguments();

        // Change to new array of actions
        int index = 0;
        if (arraysOfActions.length > 1) for (int i = 0; i < arraysOfActions.length; i++) if (Arrays.equals(actions, arraysOfActions[i])) {
            index = i + 1 < arraysOfActions.length ? i + 1 : 0;
            break;
        }//if

        getArguments().putParcelableArray(INTERNAL_EXTRA_ACTIONS, arraysOfActions[index]);
        actionAdapter.changeActions(arraysOfActions[index]);
    }//changeToNextActionGroup()

    /**
     * Changes action group.
     *
     * @param index action group index.
     */
    private void changeActionGroup(final int index) {
        getArguments().putParcelableArray(INTERNAL_EXTRA_ACTIONS, arraysOfActions[index]);
        actionAdapter.changeActions(arraysOfActions[index]);
    }//changeActionGroup()

    /**
     * Changes to first action group.
     */
    public void changeToFirstActionGroup() {
        changeActionGroup(0);
    }//changeToFirstActionGroup()

    /**
     * Changes to last action group.
     */
    public void changeToLastActionGroup() {
        changeActionGroup(arraysOfActions.length - 1);
    }//changeToLastActionGroup()

    /**
     * Click listener for all buttons.
     */
    private final View.OnClickListener buttonsOnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.cmd_view__refresh: {
                final FragmentActivity activity = getActivity();
                if (activity != null) activity.onBackPressed();

                break;
            }//REFRESH
            }
        }//onClick()

    };//buttonsOnClickListener

    /**
     * Action adapter.
     */
    private class ActionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private Action[] actions;

        /**
         * Makes new instance.
         *
         * @param actions the actions.
         */
        public ActionAdapter(@NonNull Action[] actions) {
            this.actions = actions;
        }//ActionAdapter()

        @Override
        public long getItemId(int position) {
            return position;
        }//getItemId()

        @Override
        public int getItemViewType(int position) {
            return position % 2 == 0 ? R.layout.list_item__fragment__home_menu__cmd : R.layout.list_item__fragment__home_menu__cmd_separator;
        }//getItemViewType()

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
            switch (viewType) {
            case R.layout.list_item__fragment__home_menu__cmd: return new ActionViewHolder(itemView);
            case R.layout.list_item__fragment__home_menu__cmd_separator: return new SeparatorViewHolder(itemView);
            }

            return null;
        }//onCreateViewHolder()

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof ActionViewHolder) bindActionViewHolder((ActionViewHolder) holder);
        }//onBindViewHolder()

        /**
         * Binds action view holder.
         *
         * @param holder action view holder.
         */
        private void bindActionViewHolder(@NonNull final ActionViewHolder holder) {
            final Action action = actions[holder.getAdapterPosition() / 2];
            holder.textTitle.setText(action.title);
            holder.textTitle.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Bundle fragmentArgs;

                    //TODO
                    switch (action.id) {
                    case R.id.action__tongue_twisters: {
                        // Open screen of Tongue Twisters
                        final Bundle args = new Bundle();
                        args.putBoolean(FragmentTongueTwisters.EXTRA_USE_HORIZONTAL_MODE, true);

                        BaseAdsActivity.newIntentBuilder(getContext())
                                .setFragment(FragmentTongueTwisters.class, args)
                                .setTitle(R.string.text__tongue_twisters)
                                .start();
                        break;
                    }//TONGUE_TWISTERS

                    case R.id.action__favorites: {
                        final Bundle args = new Bundle();
                        args.putBoolean(FragmentTongueTwisters.EXTRA_FAVORITES_ONLY, true);

                        BaseAdsActivity.newIntentBuilder(getContext())
                                .setFragment(FragmentTongueTwisters.class, args)
                                .setTitle(R.string.text__tongue_twisters)
                                .start();

                        break;
                    }//FAVORITES

                    case R.id.action__create: {
                        BaseAdsActivity.newIntentBuilder(getContext())
                                .setFragment(TongueTwisterCreatorFragment.class)
                                .setTitle(R.string.create)
                                .start();
                        break;
                    }//CREATE

                    case R.id.action__challenge_mode: {
                        final Bundle args = new Bundle();
                        args.putBoolean(FragmentTongueTwisters.EXTRA_CHALLENGE_MODE, true);

                        BaseAdsActivity.newIntentBuilder(getContext())
                                .setFragment(FragmentTongueTwisters.class, args)
                                .setTitle(R.string.text__tongue_twisters)
                                .start();

                        break;
                    }//CHALLENGE_MODE

                    case R.id.action__tongue_twister_recordings: {
                        BaseAdsActivity.newIntentBuilder(getContext())
                                .setFragment(FragmentTongueTwisterRecordings.class)
                                .setTitle(R.string.recordings)
                                .start();

                        break;
                    }//TONGUE_TWISTERS_RECORDINGS

                    case R.id.action__actor_warm_up_video: {
                        fragmentArgs = new Bundle();
                        fragmentArgs.putInt(FragmentVideoResourcePlayer.EXTRA_VIDEO_RESOURCE, R.raw.video__0);

                        BaseAdsActivity.newIntentBuilder(getContext())
                                .setScreenOrientation(ActivityInfo.SCREEN_ORIENTATION_USER)
                                .setPadding(BaseAdsActivity.Padding.ALL)
                                .setFragment(FragmentVideoResourcePlayer.class, fragmentArgs)
                                .setTitle(action.title)
                                .start();

                        break;
                    }//ACTOR_WARM_UP_VIDEO

                    case R.id.action__actor_voice: {
                        fragmentArgs = new Bundle();
                        fragmentArgs.putInt(FragmentAudioResourcePlayer.EXTRA_AUDIO_RESOURCE, R.raw.mp3__the_actor_warmup);

                        BaseAdsActivity.newIntentBuilder(getContext())
                                .setPadding(BaseAdsActivity.Padding.ALL)
                                .setFragment(FragmentAudioResourcePlayer.class, fragmentArgs)
                                .setTitle(action.title)
                                .start();

                        break;
                    }//ACTOR_VOICE

                    case R.id.action__singer_warm_up: {
                        fragmentArgs = new Bundle();
                        fragmentArgs.putInt(FragmentAudioResourcePlayer.EXTRA_AUDIO_RESOURCE, R.raw.mp3__singing_warmup);

                        BaseAdsActivity.newIntentBuilder(getContext())
                                .setPadding(BaseAdsActivity.Padding.ALL)
                                .setFragment(FragmentAudioResourcePlayer.class, fragmentArgs)
                                .setTitle(action.title)
                                .start();

                        break;
                    }//SINGER_WARM_UP
                    }
                }//onClick()

            });
        }//bindActionViewHolder()

        @Override
        public int getItemCount() {
            return actions.length * 2;
        }//getItemCount()

        /**
         * Changes actions.
         *
         * @param actions the new actions.
         */
        public void changeActions(@NonNull Action... actions) {
            this.actions = actions;
            notifyDataSetChanged();
        }//changeActions()

        /**
         * Gets actions.
         *
         * @return the actions.
         */
        @NonNull
        public Action[] getActions() {
            return actions;
        }//getActions()

        /**
         * Action view holder.
         */
        protected class ActionViewHolder extends RecyclerView.ViewHolder {

            public final TextView textTitle;

            /**
             * Makes new instance.
             *
             * @param itemView item view.
             */
            public ActionViewHolder(View itemView) {
                super(itemView);

                // MAP CONTROLS

                textTitle = Views.findById(itemView, R.id.text__title);
            }//ActionViewHolder()

        }//ActionViewHolder

        /**
         * Separator view holder.
         */
        protected class SeparatorViewHolder extends RecyclerView.ViewHolder {

            /**
             * Makes new instance.
             *
             * @param itemView item view.
             */
            public SeparatorViewHolder(View itemView) {
                super(itemView);
            }//SeparatorViewHolder()

        }//SeparatorViewHolder

    }//ActionAdapter

}