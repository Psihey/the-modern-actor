package app.fragments.home;

import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.R;
import app.fragments.base.BaseFragment;
import app.widgets.ControlPanelView;

import haibison.android.fad7.utils.Views;

/**
 * Fragment Home.
 * <p>
 * The fragment sends {@link #MSG_CMD_CLICKED} to any registered message handlers.
 */
public class FragmentHome extends BaseFragment {

    /**
     * This message is sent when a {@link ControlPanelView.Button} is clicked. The button ID will be stored in {@link Message#arg1}.
     */
    public static final int MSG_CMD_CLICKED = MSG_FIRST_USER;

    private ControlPanelView controlPanelView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View contentView = inflater.inflate(R.layout.fragment__home, container, false);

        // MAP CONTROLS

        controlPanelView = Views.findById(contentView, R.id.control_panel);

        return contentView;
    }// onCreateView()

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // SETUP CONTROLS

        controlPanelView.setOnClickListener(controlPanelViewOnClickListener);
    }//onViewCreated()

    ////////////
    // LISTENERS
    ////////////

    /**
     * Click listener for control panel.
     */
    private final View.OnClickListener controlPanelViewOnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            sendMsg(MSG_CMD_CLICKED, controlPanelView.getTouchedButton().ordinal());
        }//onClick()

    };//controlPanelViewOnClickListener

}