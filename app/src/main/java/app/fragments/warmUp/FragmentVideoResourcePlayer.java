package app.fragments.warmUp;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.RawRes;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import app.R;
import app.widgets.MediaController;

import haibison.android.fad7.utils.Views;

import static app.BuildConfig.TAG;

/**
 * Video resource player fragment.
 */
public class FragmentVideoResourcePlayer extends BaseMediaPlayerFragment {

    private static final String CLASSNAME = FragmentVideoResourcePlayer.class.getName();

    /**
     * Use this extra to provide video resource.
     * <p>
     * Type: video resource ID, or {@link Uri}.
     */
    public static final String EXTRA_VIDEO_RESOURCE = CLASSNAME + ".VIDEO_RESOURCE";

    /**
     * Use this extra to specify whether to play the audio resource on start.
     * <p>
     * Type: boolean. Default is {@code true}.
     */
    public static final String EXTRA_PLAY_ON_START = CLASSNAME + ".PLAY_ON_START";

    private SurfaceView surfaceView;
    private MediaController mediaController;

    /**
     * Sets {@link #EXTRA_VIDEO_RESOURCE}.
     *
     * @param resId see {@link #EXTRA_VIDEO_RESOURCE}.
     * @return this fragment.
     */
    public <T extends FragmentVideoResourcePlayer> T setVideoResource(@RawRes int resId) {
        getArguments().putInt(EXTRA_VIDEO_RESOURCE, resId);
        return (T) this;
    }//setVideoResource()

    /**
     * Sets {@link #EXTRA_VIDEO_RESOURCE}.
     *
     * @param uri see {@link #EXTRA_VIDEO_RESOURCE}.
     * @return this fragment.
     */
    public <T extends FragmentVideoResourcePlayer> T setVideoResource(@NonNull Uri uri) {
        getArguments().putParcelable(EXTRA_VIDEO_RESOURCE, uri);
        return (T) this;
    }//setVideoResource()

    /**
     * Sets {@link #EXTRA_PLAY_ON_START}.
     *
     * @param flag see {@link #EXTRA_PLAY_ON_START}.
     * @return this fragment.
     */
    public <T extends FragmentVideoResourcePlayer> T setPlayOnStart(boolean flag) {
        getArguments().putBoolean(EXTRA_PLAY_ON_START, flag);
        return (T) this;
    }//setAudioResource()

    /**
     * You can override this method to provide your own layout. It must contain {@link MediaController} with ID of
     * {@link R.id#fragment__audio_resource_player__media_controller}.
     *
     * @return layout resource ID. Default is {@link R.layout#fragment__audio_resource_player}.
     */
    @LayoutRes
    protected int getFragmentLayout() {
        return R.layout.fragment__video_resource_player;
    }//getFragmentLayout()

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View contentView = inflater.inflate(getFragmentLayout(), container, false);

        // MAP CONTROLS

        surfaceView = Views.findById(contentView, R.id.fragment__video_resource_player__surface);
        mediaController = Views.findById(contentView, R.id.fragment__video_resource_player__media_controller);

        return contentView;
    }//onCreateView()

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // SETUP CONTROLS

        mediaController.setMediaPlayerArrogantHelper(mediaPlayerArrogantHelper);

        final SurfaceHolder surfaceHolder = surfaceView.getHolder();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        surfaceHolder.removeCallback(surfaceHolderCallback);
        surfaceHolder.addCallback(surfaceHolderCallback);
    }//onViewCreated()

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        MediaPlayer mediaPlayer = null;

        final Object videoResource = getArguments().get(EXTRA_VIDEO_RESOURCE);
        final boolean playOnStart = getArguments().getBoolean(EXTRA_PLAY_ON_START, true);
        if (videoResource instanceof Integer)
            mediaController.setMediaPlayer(mediaPlayer = newMediaPlayer((Integer) videoResource, playOnStart));
        else if (videoResource instanceof Uri)
            mediaController.setMediaPlayer(mediaPlayer = newMediaPlayer((Uri) videoResource, playOnStart));

        // Setup media player
        if (mediaPlayer != null) mediaPlayer.setScreenOnWhilePlaying(true);
    }//onActivityCreated()

    /**
     * Callback for surface holder.
     */
    private final SurfaceHolder.Callback surfaceHolderCallback = new SurfaceHolder.Callback() {

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            final MediaPlayer mediaPlayer = getMediaPlayer();
            if (mediaPlayer != null) mediaPlayer.setDisplay(holder);
        }//surfaceCreated()

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Log.d(TAG, "surfaceChanged() >> frame=" + holder.getSurfaceFrame() + " >> " + width + 'x' + height);

            final MediaPlayer mediaPlayer = getMediaPlayer();
            if (mediaPlayer != null) {
                final int videoWidth = mediaPlayer.getVideoWidth();
                final int videoHeight = mediaPlayer.getVideoHeight();

                if (videoHeight != 0 && height != 0 && Math.abs((float) videoWidth / videoHeight - (float) width / height) > 1) {
                    final int minTargetSize = Math.min(width, height);
                    final int maxVideoSize = Math.max(videoWidth, videoHeight);
                    int newWidth, newHeight;

                    if (minTargetSize >= maxVideoSize) {
                        newWidth = videoWidth;
                        newHeight = videoHeight;
                    } else {
                        final float ratio = (float) minTargetSize / maxVideoSize;
                        newWidth = (int) (videoWidth * ratio);
                        newHeight = (int) (videoHeight * ratio);
                    }

                    holder.setFixedSize(newWidth, newHeight);
                    Log.d(TAG, "new size = " + newWidth + 'x' + newHeight);
                }//if
            }//if
        }//surfaceChanged()

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            final MediaPlayer mediaPlayer = getMediaPlayer();
            if (mediaPlayer != null) mediaPlayer.setDisplay(null);
        }//surfaceDestroyed()

    };//surfaceHolderCallback

    /**
     * Media player arrogant helper for {@link MediaController}.
     */
    private final MediaController.MediaPlayerArrogantHelper mediaPlayerArrogantHelper = new MediaController.MediaPlayerArrogantHelper() {

        @Override
        public boolean isPlaying() {
            final SurfaceHolder surfaceHolder = surfaceView.getHolder();
            if (surfaceHolder == null) return false;

            final MediaPlayer mp = getMediaPlayer();
            try {
                return mp == null ? false : mp.isPlaying();
            } catch (Throwable t) {
                return false;
            }
        }//isPlaying()

    };//

}