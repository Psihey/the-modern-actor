package app.fragments.warmUp;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.RawRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.R;
import app.widgets.MediaController;

import haibison.android.fad7.utils.Views;

/**
 * Audio resource player fragment.
 */
public class FragmentAudioResourcePlayer extends BaseMediaPlayerFragment {

    private static final String CLASSNAME = FragmentAudioResourcePlayer.class.getName();

    /**
     * Use this extra to provide audio resource.
     * <p>
     * Type: audio resource ID, or {@link Uri}.
     */
    public static final String EXTRA_AUDIO_RESOURCE = CLASSNAME + ".AUDIO_RESOURCE";

    /**
     * Use this extra to specify whether to play the audio resource on start.
     * <p>
     * Type: boolean. Default is {@code true}.
     */
    public static final String EXTRA_PLAY_ON_START = CLASSNAME + ".PLAY_ON_START";

    private MediaController mediaController;

    /**
     * Sets {@link #EXTRA_AUDIO_RESOURCE}.
     *
     * @param resId see {@link #EXTRA_AUDIO_RESOURCE}.
     * @return this fragment.
     */
    public <T extends FragmentAudioResourcePlayer> T setAudioResource(@RawRes int resId) {
        getArguments().putInt(EXTRA_AUDIO_RESOURCE, resId);
        return (T) this;
    }//setAudioResource()

    /**
     * Sets {@link #EXTRA_AUDIO_RESOURCE}.
     *
     * @param uri see {@link #EXTRA_AUDIO_RESOURCE}.
     * @return this fragment.
     */
    public <T extends FragmentAudioResourcePlayer> T setAudioResource(@NonNull Uri uri) {
        getArguments().putParcelable(EXTRA_AUDIO_RESOURCE, uri);
        return (T) this;
    }//setAudioResource()

    /**
     * Sets {@link #EXTRA_PLAY_ON_START}.
     *
     * @param flag see {@link #EXTRA_PLAY_ON_START}.
     * @return this fragment.
     */
    public <T extends FragmentAudioResourcePlayer> T setPlayOnStart(boolean flag) {
        getArguments().putBoolean(EXTRA_PLAY_ON_START, flag);
        return (T) this;
    }//setAudioResource()

    /**
     * You can override this method to provide your own layout. It must contain {@link MediaController} with ID of
     * {@link R.id#fragment__audio_resource_player__media_controller}.
     *
     * @return layout resource ID. Default is {@link R.layout#fragment__audio_resource_player}.
     */
    @LayoutRes
    protected int getFragmentLayout() {
        return R.layout.fragment__audio_resource_player;
    }//getFragmentLayout()

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View contentView = inflater.inflate(getFragmentLayout(), container, false);

        // MAP CONTROLS

        mediaController = Views.findById(contentView, R.id.fragment__audio_resource_player__media_controller);

        return contentView;
    }//onCreateView()

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Object audioResource = getArguments().get(EXTRA_AUDIO_RESOURCE);
        final boolean playOnStart = getArguments().getBoolean(EXTRA_PLAY_ON_START, true);
        if (audioResource instanceof Integer) mediaController.setMediaPlayer(newMediaPlayer((Integer) audioResource, playOnStart));
        else if (audioResource instanceof Uri) mediaController.setMediaPlayer(newMediaPlayer((Uri) audioResource, playOnStart));
    }//onActivityCreated()

}