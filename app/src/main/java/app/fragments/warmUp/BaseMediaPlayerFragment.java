package app.fragments.warmUp;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import app.fragments.base.BaseFragment;

import static app.BuildConfig.TAG;

/**
 * Base media player fragment.
 */
public class BaseMediaPlayerFragment extends BaseFragment {

    private static final String CLASSNAME = BaseMediaPlayerFragment.class.getName();

    private MediaPlayer mediaPlayer;

    @Override
    public void onPause() {
        super.onPause();

        final MediaPlayer mediaPlayer = getMediaPlayer();
        if (mediaPlayer != null) try {
            if (mediaPlayer.isPlaying()) mediaPlayer.pause();
        } catch (Throwable t) {
            Log.e(TAG, t.getMessage(), t);
        }
    }// onPause()

    @Override
    public void onDestroy() {
        releaseMediaPlayer();

        super.onDestroy();
    }// onDestroy()

    /**
     * Makes new media player. Note that current instance, if not {@code null}, will be released.
     *
     * @param mediaResourceId media resource ID.
     * @param play            {@code true} or {@code false}.
     * @return the media player.
     */
    @NonNull
    protected MediaPlayer newMediaPlayer(int mediaResourceId, boolean play) {
        // Release current instance if not null
        releaseMediaPlayer();

        mediaPlayer = MediaPlayer.create(getActivity(), mediaResourceId);
        setupMediaPlayer(play);

        return mediaPlayer;
    }// newMediaPlayer()

    /**
     * Makes new media player and plays given resource. Note that current instance, if not {@code null}, will be released.
     *
     * @param mediaResourceId media resource ID.
     * @return the media player.
     */
    @NonNull
    protected MediaPlayer newMediaPlayer(int mediaResourceId) {
        return newMediaPlayer(mediaResourceId, true);
    }//newMediaPlayer()

    /**
     * Makes new media player. Note that current instance, if not {@code null}, will be released.
     *
     * @param mediaUri media URI.
     * @param play     {@code true} or {@code false}.
     * @return the media player.
     */
    @NonNull
    protected MediaPlayer newMediaPlayer(@NonNull Uri mediaUri, boolean play) {
        // Release current instance if not null
        releaseMediaPlayer();

        mediaPlayer = MediaPlayer.create(getActivity(), mediaUri);
        setupMediaPlayer(play);

        return mediaPlayer;
    }// newMediaPlayer()

    /**
     * Makes new media player and plays given resource. Note that current instance, if not {@code null}, will be released.
     *
     * @param mediaUri media URI.
     * @return the media player.
     */
    @NonNull
    protected MediaPlayer newMediaPlayer(@NonNull Uri mediaUri) {
        return newMediaPlayer(mediaUri, true);
    }//newMediaPlayer()

    /**
     * Sets up the media player.
     *
     * @param startWhenPrepared {@code true} or {@code false}.
     */
    protected void setupMediaPlayer(final boolean startWhenPrepared) {
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mp) {
                try {
                    if (startWhenPrepared) mp.start();
                } catch (Throwable t) {
                    Log.e(TAG, t.getMessage(), t);
                }
            }// onPrepared()

        });
    }//setupMediaPlayer()

    /**
     * Gets media player.
     *
     * @return the media player.
     */
    @Nullable
    protected MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }// getMediaPlayer()

    /**
     * Releases media player.
     */
    protected void releaseMediaPlayer() {
        if (mediaPlayer != null) try {
            mediaPlayer.release();
        } catch (Throwable t) {
            Log.e(TAG, t.getMessage(), t);
        } finally {
            mediaPlayer = null;
        }
    }// releaseMediaPlayer()

}