package app.fragments.base;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import app.R;
import haibison.android.fad7.utils.Views;

import static android.text.format.DateUtils.SECOND_IN_MILLIS;

/**
 * Recycler fragment -- which makes use of {@link RecyclerView}. You can use
 * your arbitrary layout via {@link #getResourceLayoutId()}, but please include
 * this one in yours: {@link R.layout#recycler_fragment}.
 */
public abstract class RecyclerFragment extends BaseFragment {

    private View contentView;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private TextView textMessage;

    /**
     * Creates new layout manager for the recycler view.
     *
     * @param context the context.
     * @return the layout manager.
     */
    protected abstract RecyclerView.LayoutManager newLayoutManager(Context context);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(getResourceLayoutId(), container, false);

        // MAP CONTROLS

        contentView = rootView.findViewById(R.id.recycler_fragment__content);
        recyclerView = Views.findById(rootView, R.id.recycler_fragment__recycler);
        textMessage = Views.findById(rootView, R.id.recycler_fragment__text__message);
        progressBar = Views.findById(rootView, R.id.recycler_fragment__progress_bar);

        // SETUP CONTROLS

        recyclerView.setLayoutManager(newLayoutManager(inflater.getContext()));

        return rootView;
    }//onCreateView()

    /**
     * Gets resource layout ID. Default is {@link R.layout#recycler_fragment}.
     *
     * @return resource layout ID.
     */
    protected int getResourceLayoutId() {
        return R.layout.recycler_fragment;
    }// getResourceLayoutId()

    /**
     * Gets the recycler view.
     *
     * @return the recycler view. This will be valid only after
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     */
    public RecyclerView getRecyclerView() {
        return recyclerView;
    }// getRecyclerView()

    /**
     * Sets adapter for the recycler view. Note that you can only call this
     * method after {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     *
     * @param adapter the adapter.
     */
    public void setAdapter(Adapter<?> adapter) {
        final Adapter<?> oldAdapter = getRecyclerView().getAdapter();
        if (oldAdapter != null) oldAdapter.unregisterAdapterDataObserver(mAdapterDataObserver);

        if (adapter != null) adapter.registerAdapterDataObserver(mAdapterDataObserver);
        getRecyclerView().setAdapter(adapter);

        updateEmptyTextView();
    }// setAdapter()

    /**
     * Hides the group view containing the recycler view, then shows the
     * progress bar. Note that you can only call this method after
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     */
    public void showProgressBar() {
        contentView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }// showProgressBar()

    /**
     * Hides the progress bar, then shows the group view containing the recycler
     * view. Note that you can only call this method after
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     */
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
        contentView.setVisibility(View.VISIBLE);
    }// hideProgressBar()

    /**
     * Sets empty text, which will be shown if there is no data. Note that you
     * can only call this method after
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     *
     * @param resId string resource ID.
     */
    public void setEmptyText(int resId) {
        textMessage.setText(resId);
        updateEmptyTextView();
    }// setEmptyText()

    /**
     * Sets empty text, which will be shown if there is no data. Note that you
     * can only call this method after
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     *
     * @param text string.
     */
    public void setEmptyText(CharSequence text) {
        textMessage.setText(text);
        updateEmptyTextView();
    }// setEmptyText()

    /**
     * Updates empty text view.
     */
    protected void updateEmptyTextView() {
        final Adapter<?> adapter = getRecyclerView().getAdapter();
        if (adapter == null || adapter.getItemCount() == 0) {
            getRecyclerView().setVisibility(View.GONE);
            textMessage.setVisibility(View.VISIBLE);
        } else {
            textMessage.setVisibility(View.GONE);
            getRecyclerView().setVisibility(View.VISIBLE);
        }
    }// updateEmptyTextView()

    /***********
     * LISTENERS
     ***********/

    /**
     * Data observer for the adapter.
     */
    private final RecyclerView.AdapterDataObserver mAdapterDataObserver = new RecyclerView.AdapterDataObserver() {

        @Override
        public void onChanged() {
            updateEmptyTextView();
        }// onChanged()

    };// mAdapterDataObserver

    /**
     * Base cursor adapter.
     * <p>
     * <h1>Usage</h1>
     * <ul>
     * <li>If you have a lot of updates, consider using {@link #smoothNotifyDataSetChanged()} instead of
     * {@link #notifyDataSetChanged()}.</li>
     * </ul>
     */
    public static abstract class BaseCursorAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

        /**
         * Minimum time to smoothly update data set, in milliseconds.
         */
        private static final long MIN_PERIOD_DATA_SET_SMOOTH_UPDATE = SECOND_IN_MILLIS / 4;

        private static final int MSG_UPDATE_DATA_SET = 0;

        private final Handler mHandler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                case MSG_UPDATE_DATA_SET: {
                    notifyDataSetChanged();
                    break;
                }// MSG_UPDATE_DATA_SET
                }
            }// handleMessage()

        };// mHandler

        private final Context context;
        private Cursor cursor;

        /**
         * Makes new instance.
         *
         * @param context the context, maybe {@code null} (based on your own use).
         */
        public BaseCursorAdapter(@Nullable Context context) {
            this.context = context;
        }// BaseCursorAdapter()

        @Override
        public int getItemCount() {
            return cursor == null ? 0 : cursor.getCount();
        }// getItemCount()

        /**
         * Gets the context which was passed in constructor.
         *
         * @return the context, maybe {@code null}.
         */
        @Nullable
        public Context getContext() {
            return context;
        }// getContext()

        /**
         * Changes cursor <em>synchronously</em>.
         *
         * @param cursor the new cursor, maybe {@code null}.
         */
        public synchronized void changeCursor(@Nullable Cursor cursor) {
            if (this.cursor != null) this.cursor.close();
            this.cursor = cursor;
            notifyDataSetChanged();
        }// changeCursor()

        /**
         * Gets cursor.
         *
         * @return the cursor.
         */
        @Nullable
        public Cursor getCursor() {
            return cursor;
        }// getCursor()

        /**
         * This calls {@link #notifyDataSetChanged()} smoothly.
         */
        public void smoothNotifyDataSetChanged() {
            if (mHandler.hasMessages(MSG_UPDATE_DATA_SET) == false)
                mHandler.sendEmptyMessageDelayed(MSG_UPDATE_DATA_SET, MIN_PERIOD_DATA_SET_SMOOTH_UPDATE);
        }// smoothNotifyDataSetChanged()

    }// BaseCursorAdapter

}