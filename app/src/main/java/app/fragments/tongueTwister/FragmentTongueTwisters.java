package app.fragments.tongueTwister;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView.LayoutManager;

import app.activities.BaseAdsActivity;
import app.R;
import app.adapters.AdapterTongueTwisters;
import app.fragments.tongueTwister.recording.FragmentTongueTwisterRecording;
import app.fragments.base.RecyclerFragment;
import app.providers.TongueTwistersProvider;
import app.providers.TongueTwistersProvider.TongueTwisters;
import haibison.android.res.Ids;
import haibison.android.simpleprovider.SimpleContract;
import haibison.android.simpleprovider.utils.Strings;
import haibison.android.underdogs.Param;

import static haibison.android.simpleprovider.SimpleContract.TRUE_AS_INT;

/**
 * Fragment Tongue Twisters.
 */
public class FragmentTongueTwisters extends RecyclerFragment implements LoaderCallbacks<Cursor> {

    private static final String CLASSNAME = FragmentTongueTwisters.class.getName();

    /**
     * Flag for using horizontal mode. Default is {@code false}.
     */
    @Param(type = Param.Type.INPUT, dataTypes = boolean.class)
    public static final String EXTRA_USE_HORIZONTAL_MODE = CLASSNAME + ".USE_HORIZONTAL_MODE";

    /**
     * Flag for favorite tongue twisters only.
     */
    @Param(type = Param.Type.INPUT, dataTypes = boolean.class)
    public static final String EXTRA_FAVORITES_ONLY = CLASSNAME + ".FAVORITES_ONLY";

    /**
     * Challenge mode.
     */
    @Param(type = Param.Type.INPUT, dataTypes = boolean.class)
    public static final String EXTRA_CHALLENGE_MODE = CLASSNAME + ".CHALLENGE_MODE";

    private final int loaderIdData = Ids.newUid();

    private AdapterTongueTwisters adapterTongueTwisters;

    /**
     * Sets {@link #EXTRA_FAVORITES_ONLY}.
     *
     * @param flag see {@link #EXTRA_FAVORITES_ONLY}.
     * @return this fragment.
     */
//    public <T extends FragmentTongueTwisters> T setFavoritesOnly(boolean flag) {
//        getArguments().putBoolean(EXTRA_FAVORITES_ONLY, flag);
//        return (T) this;
//    }//setFavoritesOnly()

    /**
     * Gets {@link #EXTRA_FAVORITES_ONLY}.
     *
     * @return {@code true/false}.
     */
    public boolean areFavoritesOnly() {
        return getArguments().getBoolean(EXTRA_FAVORITES_ONLY, false);
    }//areFavoritesOnly()

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setEmptyText(R.string.text__no_data);

        adapterTongueTwisters = new AdapterTongueTwisters(getActivity());
        adapterTongueTwisters.setEventListener(adapterTongueTwistersEventListener);
        setAdapter(adapterTongueTwisters);
        getLoaderManager().initLoader(loaderIdData, null, this);
    }// onActivityCreated()



    @Override
    public void onDestroy() {
        adapterTongueTwisters.changeCursor(null);

        super.onDestroy();
    }// onDestroy()

    /******************
     * RecyclerFragment
     ******************/

    @Override
    protected LayoutManager newLayoutManager(Context context) {
        return new LinearLayoutManager(context);
    }// newLayoutManager()

    /*****************
     * LoaderCallbacks
     *****************/

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == loaderIdData) {
            showProgressBar();
            final String selection = areFavoritesOnly() ? Strings.join(TongueTwisters.COLUMN_FAVORITE, '=', TRUE_AS_INT) : null;
            return new CursorLoader(
                    getActivity(), SimpleContract.getContentUri(getContext(), TongueTwistersProvider.class, TongueTwisters.class),
                    null, selection, null, null
            );

        }// loaderIdData()



        return null;
    }// onCreateLoader()


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (loader.getId() == loaderIdData) {
            adapterTongueTwisters.changeCursor(cursor);
            hideProgressBar();
        }// loaderIdData
    }// onLoadFinished()

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (loader.getId() == loaderIdData) {
            adapterTongueTwisters.changeCursor(null);
        }// loaderIdData
    }// onLoaderReset()

    /**
     * Event listener for adapter Tongue Twisters.
     */
    private final AdapterTongueTwisters.EventListener adapterTongueTwistersEventListener = new AdapterTongueTwisters.EventListener() {

        @Override
        public void onItemClick(long tongueTwisterId) {
            final Bundle args = new Bundle();

            if (getArguments().getBoolean(EXTRA_USE_HORIZONTAL_MODE, false)) {
                args.putLong(HorizontalTongueTwistersFragment.EXTRA_SELECTED_ID, tongueTwisterId);

                BaseAdsActivity.newIntentBuilder(getContext()).setFragment(HorizontalTongueTwistersFragment.class, args).start();
            } else {
                args.putLong(FragmentTongueTwisterRecording.EXTRA_TONGUE_TWISTER_ID, tongueTwisterId);
                args.putBoolean(FragmentTongueTwisterRecording.EXTRA_CHALLENGE_MODE, getArguments().getBoolean(EXTRA_CHALLENGE_MODE, false));
                BaseAdsActivity.newIntentBuilder(getContext()).setFragment(FragmentTongueTwisterRecording.class, args).start();
            }
        }// onItemClick()

        @Override
        public void onItemLongClick(long tongueTwisterId) {
            // TODO There's no design, maybe we don't need this?
            if (true) return;

            final Bundle args = new Bundle();
            args.putLong(FragmentTongueTwisterEditor.EXTRA_TONGUE_TWISTER_ID, tongueTwisterId);

            BaseAdsActivity.newIntentBuilder(getContext()).setFragment(FragmentTongueTwisterEditor.class, args).start();
        }// onItemLongClick()

    };// adapterTongueTwistersEventListener

}