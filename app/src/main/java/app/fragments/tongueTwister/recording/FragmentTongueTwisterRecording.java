package app.fragments.tongueTwister.recording;

import android.Manifest;
import android.annotation.SuppressLint;
import android.database.Cursor;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;

import app.activities.BaseActivity;
import app.activities.BaseAdsActivity;
import app.R;
import app.fragments.base.BaseFragment;
import app.providers.TongueTwistersProvider;
import app.providers.TongueTwistersProvider.TongueTwisters;

import haibison.android.fad7.ActivityWithFragments;
import haibison.android.fad7.utils.Views;
import haibison.android.res.Ids;
import haibison.android.simpleprovider.SimpleContract;
import haibison.android.simpleprovider.utils.Strings;
import haibison.android.tfp.TempFileProvider;
import haibison.android.underdogs.Param;
import haibison.android.wls.ToastsService;

import static android.text.format.DateUtils.MINUTE_IN_MILLIS;
import static android.text.format.DateUtils.SECOND_IN_MILLIS;
import static android.text.format.DateUtils.WEEK_IN_MILLIS;

import static app.BuildConfig.TAG;

import static haibison.android.simpleprovider.utils.Chars.DOT;

/**
 * Fragment Tongue Twister Recording. You have to provide
 * {@link #EXTRA_TONGUE_TWISTER_ID} in order to use this fragment.
 */
public class FragmentTongueTwisterRecording extends BaseFragment implements LoaderCallbacks<Cursor> {

    private static final String CLASSNAME = FragmentTongueTwisterRecording.class.getName();

    /**
     * ID of tongue twister.
     */
    @Param(type = Param.Type.INPUT, required = true, dataTypes = long.class)
    public static final String EXTRA_TONGUE_TWISTER_ID = CLASSNAME + ".TONGUE_TWISTER_ID";

    /**
     * Flag to update activity title. Default is {@code true}.
     */
    @Param(type = Param.Type.INPUT, dataTypes = boolean.class)
    public static final String EXTRA_UPDATE_ACTIVITY_TITLE = CLASSNAME + ".UPDATE_ACTIVITY_TITLE";

    /**
     * Challenge mode.
     */
    @Param(type = Param.Type.INPUT, dataTypes = boolean.class)
    public static final String EXTRA_CHALLENGE_MODE = CLASSNAME + ".CHALLENGE_MODE";

    private static final long UPDATE_INTERVAL = SECOND_IN_MILLIS / 2;

    private final int loaderIdData = Ids.newUid();
    private final Handler uiHandler = new Handler();

    private TextView textTongueTwister, textStartStop, textTime;
    private Cursor cursorData;

    private MediaRecorder mediaRecorder;
    private File recordingFile;
    private long recordingStartTime, recordingDuration;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View contentView = inflater.inflate(R.layout.fragment__tongue_twister_recording, container, false);

        // MAP CONTROLS
        textStartStop = Views.findById(contentView, R.id.text__start_stop);
        textTime = Views.findById(contentView, R.id.text__time);
        textTongueTwister = Views.findById(contentView, R.id.text__tongue_twister);

        return contentView;
    }// onCreateView()

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // SETUP CONTROLS

        textStartStop.setOnClickListener(mButtonsOnClickListener);

        final boolean isChallengeMode = getArguments().getBoolean(EXTRA_CHALLENGE_MODE);
        view.findViewById(R.id.view_group__recording_controls).setVisibility(isChallengeMode ? View.VISIBLE : View.GONE);

        final FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) textTongueTwister.getLayoutParams();
        if (isChallengeMode) {
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.gravity = Gravity.TOP;

            textTongueTwister.setGravity(Gravity.TOP);
        } else {
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            layoutParams.gravity = Gravity.CENTER;

            textTongueTwister.setGravity(Gravity.CENTER);
        }
        textTongueTwister.requestLayout();
    }// onViewCreated()

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(loaderIdData, null, this);
    }// onActivityCreated()

    @Override
    public void onResume() {
        super.onResume();

        if (mediaRecorder != null) {
            uiHandler.removeCallbacks(mFinishTask);

            uiHandler.removeCallbacks(mUiUpdater);
            uiHandler.post(mUiUpdater);
        }// if
    }// onResume()

    @Override
    public void onPause() {
        super.onPause();

        if (mediaRecorder != null) {
            uiHandler.removeCallbacks(mUiUpdater);

            uiHandler.removeCallbacks(mFinishTask);
            uiHandler.post(mFinishTask);
        }// if
    }// onPause()

    @Override
    public void onDestroy() {
        uiHandler.removeCallbacks(mFinishTask);
        uiHandler.removeCallbacks(mUiUpdater);

        if (mediaRecorder != null) {
            releaseMediaRecorder();
            if (recordingFile != null) {
                recordingFile.delete();
                recordingFile = null;
            }//if
        }// if

        changeCursor(null);

        super.onDestroy();
    }// onDestroy()

    /*****************
     * LoaderCallbacks
     *****************/

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == loaderIdData) {
            return new CursorLoader(
                    getContext(),
                    SimpleContract.getContentItemUri(
                            getContext(), TongueTwistersProvider.class, TongueTwisters.class, getArguments().getLong(EXTRA_TONGUE_TWISTER_ID)),
                    null, null, null, null
            );
        }// loaderIdData

        return null;
    }// onCreateLoader()

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == loaderIdData) {
            changeCursor(data);
            if (cursorData != null && cursorData.moveToFirst()) {
                final String tongueTwister = cursorData.getString(cursorData.getColumnIndex(TongueTwisters.COLUMN_TONGUE_TWISTER));
                if (getArguments().getBoolean(EXTRA_CHALLENGE_MODE, false)) {
                    final String s = tongueTwister.endsWith(Character.toString(DOT)) ? tongueTwister : tongueTwister + DOT;
                    textTongueTwister.setText(Strings.join(s, s, s));
                } else textTongueTwister.setText(tongueTwister);
            } else {
                ToastsService.toastLong(getActivity(), R.string.msg__unknown_error_try_again);
                getActivity().finish();
            }
        }// loaderIdData
    }// onLoadFinished()

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (loader.getId() == loaderIdData) {
            changeCursor(null);
        }// loaderIdData
    }// onLoaderReset()

    /**
     * Changes cursor. Existing cursor will be closed.
     */
    private void changeCursor(final Cursor cursor) {
        if (cursorData != null) cursorData.close();
        cursorData = cursor;

        if (cursor != null && cursor.moveToFirst())
            textTongueTwister.setText(cursor.getString(cursor.getColumnIndex(TongueTwisters.COLUMN_TONGUE_TWISTER)));
        else
            textTongueTwister.setText(null);

        // Update activity title
//        if (getArguments().getBoolean(EXTRA_UPDATE_ACTIVITY_TITLE, true)) {
//            final FragmentActivity activity = getActivity();
//            if (activity != null) activity.setTitle("ffff");
//        }//if
    }// changeCursor()

    /**
     * Releases media recorder.
     */
    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            try {
                recordingDuration = System.currentTimeMillis() - recordingStartTime;

                mediaRecorder.stop();
                mediaRecorder.release();
            } catch (Throwable t) {
                Log.e(TAG, t.getMessage(), t);
            } finally {
                mediaRecorder = null;
            }
        }// if
    }// releaseMediaRecorder()

    /**
     * Starts recording.
     *
     * @return {@code true} if OK, {@code false} otherwise.
     */
    @SuppressLint("InlinedApi")
    private boolean startRecording() {
        recordingFile = TempFileProvider.newTempFile(getContext(), System.currentTimeMillis() + WEEK_IN_MILLIS);
        try {
            if (recordingFile.createNewFile() == false) return false;
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
            return false;
        }

        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mediaRecorder.setOutputFile(recordingFile.getAbsolutePath());
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        mediaRecorder.setOnErrorListener(new MediaRecorder.OnErrorListener() {

            @Override
            public void onError(MediaRecorder mr, int what, int extra) {
                releaseMediaRecorder();
                TempFileProvider.deleteTempFile(getContext(), recordingFile);
                ToastsService.toastLong(getActivity(), R.string.msg__error__recording_audio_try_again);
                finishActivity();
            }// onError()

        });

        try {
            mediaRecorder.prepare();
        } catch (Throwable t) {
            Log.e(TAG, t.getMessage(), t);
            releaseMediaRecorder();
            return false;
        }

        mediaRecorder.start();
        recordingStartTime = System.currentTimeMillis();

        uiHandler.post(mUiUpdater);

        return true;
    }// startRecording()

    ////////////
    // LISTENERS
    ////////////

    /**
     * Click listener for all buttons.
     */
    private final View.OnClickListener mButtonsOnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(final View v) {
            switch (v.getId()) {
            case R.id.text__start_stop: {
                if (mediaRecorder == null) {
                    final BaseActivity activity = (BaseActivity) getActivity();
                    if (activity.handlePermissionsHandler(new ActivityWithFragments.PermissionsHandler() {

                        @Override
                        public String[] getPermissions() {
                            return new String[] { Manifest.permission.RECORD_AUDIO };
                        }//getPermissions()

                        @Override
                        public CharSequence getPermissionsDetailsMessage() {
                            return getString(R.string.msg__permission_details__record_audio);
                        }//getPermissionsDetailsMessage()

                        @Override
                        public CharSequence getUserDenialExplainingMessage() {
                            return getString(R.string.msg__permission_details__record_audio__user_denied);
                        }//getUserDenialExplainingMessage()

                        @Override
                        public boolean shouldFinishWhenRequestingPermissionsNotApproved() {
                            return false;
                        }//shouldFinishWhenRequestingPermissionsNotApproved()

                        @Override
                        public void onPermissionsApproved() {
                            v.performClick();
                        }//onPermissionsApproved()

                    }) == false) {
                        v.setKeepScreenOn(true);
                        startRecording();

                        textStartStop.setText(R.string.stop);
                    }//if
                } else {
                    v.setEnabled(false);
                    v.setKeepScreenOn(false);
                    releaseMediaRecorder();

                    // Show result
                    final Bundle args = new Bundle();
                    args.putParcelable(FragmentTongueTwisterRecordingResult.EXTRA_AUDIO_RESOURCE, Uri.fromFile(recordingFile));
                    args.putLong(FragmentTongueTwisterRecordingResult.EXTRA_RECORDING_DURATION, recordingDuration);
                    args.putBoolean(FragmentTongueTwisterRecordingResult.EXTRA_PLAY_ON_START, false);
                    args.putBoolean(FragmentTongueTwisterRecordingResult.EXTRA_CHALLENGE_MODE, getArguments().getBoolean(EXTRA_CHALLENGE_MODE));
                    args.putLong(FragmentTongueTwisterRecordingResult.EXTRA_TONGUE_TWISTER_ID, getArguments().getLong(EXTRA_TONGUE_TWISTER_ID));

                    BaseAdsActivity.newIntentBuilder(getContext())
                            .setFragment(FragmentTongueTwisterRecordingResult.class, args)
                            .setTitle(R.string.text__well_done)
                            .start();

                    finishActivity();
                }

                break;
            }//START/STOP
            }
        }// onClick()

    };// mButtonsOnClickListener

    /**
     * UI updater.
     */
    private final Runnable mUiUpdater = new Runnable() {

        @Override
        public void run() {
            if (mediaRecorder == null) return;

            final long timeElapsedMillis = System.currentTimeMillis() - recordingStartTime;
            textTime.setText(String.format(
                    "%02d:%02d", timeElapsedMillis / MINUTE_IN_MILLIS, timeElapsedMillis % MINUTE_IN_MILLIS / SECOND_IN_MILLIS
            ));

            uiHandler.postDelayed(this, UPDATE_INTERVAL);
        }// run()

    };// mUiUpdater

    /**
     * Task to finish owner activity.
     */
    private final Runnable mFinishTask = new Runnable() {

        @Override
        public void run() {
            final FragmentActivity activity = getActivity();
            if (activity != null && activity.isFinishing() == false) activity.finish();
        }// run()

    };// mFinishTask

}