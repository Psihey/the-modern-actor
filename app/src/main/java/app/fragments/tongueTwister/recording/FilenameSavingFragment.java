package app.fragments.tongueTwister.recording;

import android.os.Bundle;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import app.R;
import app.fragments.base.BaseFragment;
import app.utils.Resources;
import app.utils.UI;

import haibison.android.fad7.utils.Views;
import haibison.android.underdogs.Param;

/**
 * Filename saving fragment.
 */
public class FilenameSavingFragment extends BaseFragment {

    private static final String CLASSNAME = FilenameSavingFragment.class.getName();

    /**
     * This message is sent when Save button is click. You can get the filename via {@link Message#obj}.
     */
    public static final int MSG_SAVE_BUTTON_CLICKED = MSG_FIRST_USER;

    /**
     * Filename.
     */
    @Param(type = Param.Type.OUTPUT, dataTypes = CharSequence.class)
    public static final String EXTRA_FILENAME = CLASSNAME + ".FILENAME";

    private EditText editFilename;
    private View cmdViewSave;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment__filename_saving, container, false);

        // MAP CONTROLS

        editFilename = Views.findById(rootView, R.id.edit__filename);
        cmdViewSave = rootView.findViewById(R.id.text__save);

        return rootView;
    }//onCreateView()

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // SETUP CONTROLS

        editFilename.removeTextChangedListener(editTongueTwisterTextWatcher);
        editFilename.addTextChangedListener(editTongueTwisterTextWatcher);
        editFilename.setTypeface(Resources.getTypefaceGoboldRegular(getContext()));

        cmdViewSave.setOnClickListener(commandViewsOnClickListener);
    }//onViewCreated()

    @Override
    public void onStart() {
        super.onStart();

        editFilename.post(new Runnable() {

            @Override
            public void run() {
                if (editFilename.requestFocus()) UI.showSoftInput(getContext(), editFilename, true);
            }//run()

        });
    }//onStart()

    /**
     * Text watcher for edit tongue twister.
     */
    private final TextWatcher editTongueTwisterTextWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }//beforeTextChanged()

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }//onTextChanged()

        @Override
        public void afterTextChanged(Editable s) {
            cmdViewSave.setEnabled(s.toString().trim().length() > 0);
        }//afterTextChanged()

    };//editTongueTwisterTextWatcher

    /**
     * On click listener for all command views.
     */
    private final View.OnClickListener commandViewsOnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.text__save: {
                final String filename = editFilename.getText().toString().trim();
                if (TextUtils.isEmpty(filename)) {
                    editFilename.setText(null);
                    break;
                }//if

                final Message msg = Message.obtain(null, MSG_SAVE_BUTTON_CLICKED);
                msg.getData().putCharSequence(EXTRA_FILENAME, filename);
                sendMsg(msg);

                dismiss();

                break;
            }//SAVE
            }
        }//onClick()

    };//commandViewsOnClickListener

}