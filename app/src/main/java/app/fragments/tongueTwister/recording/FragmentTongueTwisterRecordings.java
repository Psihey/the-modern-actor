package app.fragments.tongueTwister.recording;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.text.DateFormat;

import app.activities.BaseAdsActivity;
import app.R;
import app.fragments.base.RecyclerFragment;
import app.fragments.warmUp.FragmentAudioResourcePlayer;
import app.providers.TongueTwisterRecordingsProvider;
import app.providers.TongueTwisterRecordingsProvider.TongueTwisterRecordings;
import haibison.android.fad7.utils.Views;
import haibison.android.res.Ids;
import haibison.android.simpleprovider.HellFileProvider;
import haibison.android.simpleprovider.SimpleContract;
import haibison.android.simpleprovider.services.CPOExecutor;
import haibison.android.simpleprovider.utils.CPOBuilder;
import haibison.android.simpleprovider.utils.Strings;
import haibison.android.underdogs.NonNull;
import haibison.android.wls.ToastsService;

import static app.BuildConfig.TAG;
import static app.providers.TongueTwisterRecordingsProvider.TongueTwisterRecordings.COLUMN_FILE_PATH;
import static app.utils.Constants.URL_GOOGLE_PLAY_STORE_APP_PAGE;
import static app.utils.Intents.MIME_ANY_AUDIO;
import static haibison.android.simpleprovider.SimpleContract.INVALID_ID;
import static haibison.android.simpleprovider.database.BaseTable._ID;
import static haibison.android.simpleprovider.database.BaseTimingTable._DATE_MODIFIED;
import static haibison.android.simpleprovider.utils.SQLite.DESC;
import static haibison.android.simpleprovider.utils.Strings.EMPTY_STRING;

/**
 * This fragment lets the user see and share their tongue twister recordings.
 */
public class FragmentTongueTwisterRecordings extends RecyclerFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String CLASSNAME = FragmentTongueTwisterRecordings.class.getName();

    private final int idLoaderData = Ids.newUid();

    private RecordingsAdapter recordingsAdapter;

    @Override
    protected RecyclerView.LayoutManager newLayoutManager(Context context) {
        return new LinearLayoutManager(context);
    }//newLayoutManager()

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setEmptyText(R.string.text__no_recordings);
        hideProgressBar();
        recordingsAdapter = new RecordingsAdapter(getContext());
        recordingsAdapter.setHasStableIds(true);

        setAdapter(recordingsAdapter);
    }//onViewCreated()

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(idLoaderData, null, this);
    }//onActivityCreated()

    ////////////////////////////////
    // LoaderManager.LoaderCallbacks
    ////////////////////////////////

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == idLoaderData) {
            return new CursorLoader(
                    getContext(),
                    SimpleContract.getContentUri(getContext(), TongueTwisterRecordingsProvider.class, TongueTwisterRecordings.class),
                    null, null, null, Strings.join(_DATE_MODIFIED, DESC)
            );
        }//idLoaderData

        return null;
    }//onCreateLoader()

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        final int id = loader.getId();
        if (id == idLoaderData) {
            recordingsAdapter.changeCursor(data);
        }//idLoaderData
    }//onLoadFinished()

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        final int id = loader.getId();
        if (id == idLoaderData) {
            recordingsAdapter.changeCursor(null);
        }//idLoaderData
    }//onLoaderReset()

    /**
     * Recordings adapter.
     */
    private class RecordingsAdapter extends BaseCursorAdapter<RecordingsAdapter.ViewHolder> {

        private int colId, colFilePath;

        /**
         * Makes new instance.
         *
         * @param context the context, maybe {@code null} (based on your own use).
         */
        public RecordingsAdapter(@Nullable Context context) {
            super(context);
        }//RecordingsAdapter()

        @Override
        public synchronized void changeCursor(@Nullable Cursor cursor) {
            super.changeCursor(cursor);

            if (cursor != null) {
                colId = cursor.getColumnIndex(_ID);
                colFilePath = cursor.getColumnIndex(COLUMN_FILE_PATH);
            }//if
        }//changeCursor()

        @Override
        public long getItemId(int position) {
            return getCursor().moveToPosition(position) ? getCursor().getLong(colId) : INVALID_ID;
        }//getItemId()

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item__tongue_twister_recording, parent, false)
            );
        }//onCreateViewHolder()

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            // Get data
            final long itemId = getItemId(position);
            final String filepath = getCursor().getString(colFilePath);
            final File file = TextUtils.isEmpty(filepath) ? null : new File(filepath);

            final String title = file != null ? file.getName().replaceFirst("\\.[^\\.\\s]+$", EMPTY_STRING) : null;

            holder.textTitle.setText(title);
            holder.textDate.setText(
                    file != null
                            ? DateUtils.formatSameDayTime(file.lastModified(), System.currentTimeMillis(), DateFormat.SHORT, DateFormat.SHORT)
                            : null
            );
            holder.imagePlay.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    // Check the file
                    if (file == null || file.isFile() == false || file.length() <= 0) {
                        ToastsService.toastShort(getContext(), R.string.msg__recording_file_broken);
                        return;
                    }//if

                    final Bundle args = new Bundle();
                    args.putParcelable(FragmentAudioResourcePlayer.EXTRA_AUDIO_RESOURCE, Uri.fromFile(file));

                    BaseAdsActivity.newIntentBuilder(getContext())
                            .setPadding(BaseAdsActivity.Padding.ALL)
                            .setFragment(FragmentAudioResourcePlayer.class, args)
                            .setTitle(title)
                            .start();
                }//onClick()

            });
            holder.imageShare.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    if (file != null && file.isFile()) try {
                        final Uri uri = new HellFileProvider.UriBuilder(getContext(), file).setType(MIME_ANY_AUDIO).build();
                        Intent sendIntent = new Intent();
                        sendIntent
                                .setAction(Intent.ACTION_SEND)
                                .setType(MIME_ANY_AUDIO)
                                .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                                .putExtra(Intent.EXTRA_STREAM, uri)
                                .putExtra(Intent.EXTRA_TEXT,
                                        getString(R.string.pmsg__tongue_twister_recording_sharing, URL_GOOGLE_PLAY_STORE_APP_PAGE))
                                .putExtra(Intent.EXTRA_HTML_TEXT,
                                                getString(R.string.phtml__tongue_twister_recording_sharing, URL_GOOGLE_PLAY_STORE_APP_PAGE));
                                startActivity(Intent.createChooser(sendIntent,getString(R.string.share_recording_to)));
                    } catch (ActivityNotFoundException e) {
                        Log.e(TAG, e.getMessage(), e);
                    } else ToastsService.toastShort(getContext(), R.string.msg__recording_file_broken);
                }//onClick()

            });
            holder.imageMoreCommands.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    final PopupMenu menu = new PopupMenu(getContext(), view);
                    menu.inflate(R.menu.fragment__tongue_twister_recordings__list_item);
                    menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener () {

                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                            case R.id.action__delete: {
                                // Delete the file first
                                try {
                                    if (file != null && file.isFile() && file.delete() == false) {
                                        ToastsService.toastShort(getContext(), R.string.msg__unknown_error_try_again);
                                        break;
                                    }//if
                                } catch (Throwable t) {
                                    Log.e(TAG, t.getMessage(), t);
                                    ToastsService.toastShort(getContext(), R.string.msg__unknown_error_try_again);
                                    break;
                                }

                                // Now delete the record
                                final Uri itemUri = SimpleContract.getContentItemUri(
                                        getContext(), TongueTwisterRecordingsProvider.class, TongueTwisterRecordings.class, itemId
                                );
                                CPOExecutor.IntentBuilder.newBatchOperations(getContext(), null, itemUri.getAuthority())
                                        .addOperations(CPOBuilder.newDelete(itemUri).build())
                                        .start();

                                break;
                            }//delete
                            }

                            return true;
                        }//onMenuItemClick()

                    });

                    menu.show();
                }//onClick()

            });
        }//onBindViewHolder()

        /**
         * View holder.
         */
        public class ViewHolder extends RecyclerView.ViewHolder {

            @NonNull
            public final TextView textTitle, textDate;
            @NonNull
            public final ImageView imagePlay, imageShare, imageMoreCommands;

            /**
             * Makes new instance.
             *
             * @param itemView item view.
             */
            public ViewHolder(View itemView) {
                super(itemView);

                textTitle = Views.findById(itemView, R.id.list_item__tongue_twister_recording__text__title);
                textDate = Views.findById(itemView, R.id.list_item__tongue_twister_recording__text__date);
                imagePlay = Views.findById(itemView, R.id.list_item__tongue_twister_recording__image__play);
                imageShare = Views.findById(itemView, R.id.list_item__tongue_twister_recording__image__share);
                imageMoreCommands = Views.findById(itemView, R.id.list_item__tongue_twister_recording__image__more_commands);
            }//ViewHolder()

        }//ViewHolder

    }//RecordingsAdapter

}