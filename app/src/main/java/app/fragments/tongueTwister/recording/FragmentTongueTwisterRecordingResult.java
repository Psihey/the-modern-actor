package app.fragments.tongueTwister.recording;

import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Intent;
import android.database.DatabaseUtils;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.File;

import app.activities.BaseAdsActivity;
import app.R;
import app.fragments.warmUp.FragmentAudioResourcePlayer;
import app.providers.TongueTwisterRecordingsProvider;
import app.providers.TongueTwisterRecordingsProvider.TongueTwisterRecordings;
import haibison.android.fad7.Fad7;
import haibison.android.fad7.Fad7Handler;
import haibison.android.fad7.MessengerProvider;
import haibison.android.fad7.utils.Views;
import haibison.android.simpleprovider.HellFileProvider;
import haibison.android.simpleprovider.SimpleContract;
import haibison.android.simpleprovider.services.CPOExecutor;
import haibison.android.simpleprovider.utils.Strings;
import haibison.android.tfp.TempFileProvider;
import haibison.android.underdogs.NonNull;
import haibison.android.underdogs.Param;
import haibison.android.wls.ToastsService;

import static android.text.format.DateUtils.SECOND_IN_MILLIS;
import static app.BuildConfig.TAG;
import static app.providers.TongueTwisterRecordingsProvider.FILE_EXT;
import static app.providers.TongueTwisterRecordingsProvider.TongueTwisterRecordings.COLUMN_DURATION;
import static app.providers.TongueTwisterRecordingsProvider.TongueTwisterRecordings.COLUMN_FILE_PATH;
import static app.utils.Intents.MIME_ANY_AUDIO;

/**
 * Fragment tongue twister recording result.
 */
public class FragmentTongueTwisterRecordingResult extends FragmentAudioResourcePlayer implements MessengerProvider<Fad7> {

    private static final String CLASSNAME = FragmentTongueTwisterRecordingResult.class.getName();

    /**
     * Recording duration.
     * <p>
     * Type: long, in milliseconds.
     */
    public static final String EXTRA_RECORDING_DURATION = CLASSNAME + ".RECORDING_DURATION";

    /**
     * ID of tongue twister.
     * <p>
     * Type: Long, required.
     */
    public static final String EXTRA_TONGUE_TWISTER_ID = CLASSNAME + ".TONGUE_TWISTER_ID";

    /**
     * Challenge mode.
     */
    @Param(type = Param.Type.INPUT, dataTypes = boolean.class)
    public static final String EXTRA_CHALLENGE_MODE = CLASSNAME + ".CHALLENGE_MODE";

    private TextView textDuration;

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment__tongue_twister_recording_result;
    }//getFragmentLayout()

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // MAP CONTROLS

        textDuration = Views.findById(view, R.id.text__duration);

        // SETUP CONTROLS

        for (final int id : new int[]{R.id.text__share, R.id.text__retry, R.id.text__save})
            view.findViewById(id).setOnClickListener(mButtonsOnClickListener);
    }// onViewCreated()

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Update duration
        final MediaPlayer mediaPlayer = getMediaPlayer();
        final long duration = mediaPlayer != null ? mediaPlayer.getDuration() : 0L;
        getArguments().putLong(EXTRA_RECORDING_DURATION, duration);
        final int durationInSeconds = (int) (duration / SECOND_IN_MILLIS);
        textDuration.setText(getResources().getQuantityString(R.plurals.ptext__x_seconds, durationInSeconds, durationInSeconds));
    }//onActivityCreated()

    @Override
    public void onDestroy() {
        releaseMediaPlayer();

        // Delete temp recording file
        final Uri recordingFileUri = getArguments().getParcelable(EXTRA_AUDIO_RESOURCE);
        if (recordingFileUri != null) TempFileProvider.deleteTempFile(getContext(), new File(recordingFileUri.getPath()));

        super.onDestroy();
    }//onDestroy()

    /////////////////////////////
    // MessengerProvider for Fad7
    /////////////////////////////

    private static final int TASK_ID_PICK_FILENAME_TO_SAVE = 0;
    private static final int TASK_ID_PICK_FILENAME_TO_SAVE_THEN_SHARE = 1;

    @Override
    public Messenger getMessenger(@NonNull Fad7 fad7) {
        return fad7Messenger;
    }//getMessenger()

    /**
     * Message handler for {@link Fad7}.
     */
    private final Messenger fad7Messenger = new Messenger(new Fad7Handler() {

        @Override
        public void handleMessage(@NonNull final Fad7 fad7, final int taskId, @NonNull final Message msg) {
            super.handleMessage(fad7, taskId, msg);

            switch (taskId) {
            case TASK_ID_PICK_FILENAME_TO_SAVE:
            case TASK_ID_PICK_FILENAME_TO_SAVE_THEN_SHARE: {
                switch (msg.what) {
                case FilenameSavingFragment.MSG_SAVE_BUTTON_CLICKED: {
                    // Get recording file
                    final Uri recordingFileUri = getArguments().getParcelable(EXTRA_AUDIO_RESOURCE);
                    final File recordingFile = new File(recordingFileUri.getPath());

                    // Build new file
                    String newFilename = msg.getData().getCharSequence(FilenameSavingFragment.EXTRA_FILENAME).toString().trim();
                    if (newFilename.toLowerCase().endsWith(FILE_EXT.toLowerCase()) == false) newFilename += FILE_EXT;
                    final File savedRecordingFile = new File(TongueTwisterRecordingsProvider.getRecordingsDirectory(getContext()), newFilename);

                    if (recordingFile.renameTo(savedRecordingFile) == false) {
                        ToastsService.toastShort(getContext(), R.string.msg__unknown_error_try_again);
                        break;
                    }//if

                    // Check to update it first
                    final Uri contentUri = SimpleContract.getContentUri(
                            getContext(), TongueTwisterRecordingsProvider.class, TongueTwisterRecordings.class
                    );
                    final ContentValues values = new ContentValues();
                    final int update = getContext().getContentResolver().update(
                            contentUri, values,
                            Strings.join(COLUMN_FILE_PATH, '=', DatabaseUtils.sqlEscapeString(savedRecordingFile.getAbsolutePath())),
                            null
                    );
                    if (update <= 0) {
                        values.put(COLUMN_FILE_PATH, savedRecordingFile.getAbsolutePath());
                        values.put(COLUMN_DURATION, getArguments().getLong(EXTRA_RECORDING_DURATION));

                        CPOExecutor.IntentBuilder.newBulkInsert(getContext(), null, contentUri)
                                .setAuthority(contentUri.getAuthority())
                                .addValues(values)
                                .start();
                    }//if

                    if (taskId == TASK_ID_PICK_FILENAME_TO_SAVE_THEN_SHARE) try {
                        final Uri uri = new HellFileProvider.UriBuilder(getContext(), savedRecordingFile).setType(MIME_ANY_AUDIO).build();
                        Intent sendIntent = new Intent();
                        sendIntent
                                .setAction(Intent.ACTION_SEND)
                                .setType(MIME_ANY_AUDIO)
                                .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                                .putExtra(Intent.EXTRA_STREAM, uri);
                        startActivity(Intent.createChooser(sendIntent,getString(R.string.share_recording_to)));
//
                    } catch (ActivityNotFoundException e) {
                        Log.e(TAG, e.getMessage(), e);
                    }

                    // Remove the extra so we won't delete it in onDestroy()
                    getArguments().remove(EXTRA_AUDIO_RESOURCE);
                    finishActivity();

                    break;
                }//MSG_SAVE_BUTTON_CLICKED
                }

                break;
            }//TASK_ID_PICK_FILENAME_TO_SAVE,TASK_ID_PICK_FILENAME_TO_SAVE_THEN_SHARE
            }
        }//handleMessage()

    });//fad7Messenger

    /**
     * Click listener for all buttons.
     */
    private final View.OnClickListener mButtonsOnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            final int viewId = v.getId();
            switch (viewId) {
            case R.id.text__share:
            case R.id.text__save: {
                releaseMediaPlayer();

                new FilenameSavingFragment()
                        .addMessengerProviders(FragmentTongueTwisterRecordingResult.this)
                        .setTaskId(viewId == R.id.text__share ? TASK_ID_PICK_FILENAME_TO_SAVE_THEN_SHARE : TASK_ID_PICK_FILENAME_TO_SAVE)
                        .show(getFragmentManager());

                break;
            }//SHARE,SAVE

            case R.id.text__retry: {
                final Bundle args = new Bundle();
                args.putLong(FragmentTongueTwisterRecording.EXTRA_TONGUE_TWISTER_ID, getArguments().getLong(EXTRA_TONGUE_TWISTER_ID));
                args.putBoolean(FragmentTongueTwisterRecording.EXTRA_CHALLENGE_MODE, getArguments().getBoolean(EXTRA_CHALLENGE_MODE));

                BaseAdsActivity.newIntentBuilder(getContext()).setFragment(FragmentTongueTwisterRecording.class, args).start();

                finishActivity();

                break;
            }//RETRY
            }
        }// onClick()

    };// mButtonsOnClickListener

}