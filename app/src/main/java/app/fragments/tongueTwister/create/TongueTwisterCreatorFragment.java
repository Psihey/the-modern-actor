package app.fragments.tongueTwister.create;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import app.R;
import app.fragments.base.BaseFragment;
import app.providers.TongueTwistersProvider;
import app.providers.TongueTwistersProvider.TongueTwisters;
import app.utils.Resources;
import app.utils.UI;

import haibison.android.fad7.utils.Views;
import haibison.android.simpleprovider.SimpleContract;
import haibison.android.simpleprovider.services.CPOExecutor;

import static app.providers.TongueTwistersProvider.TongueTwisters.COLUMN_TITLE;
import static app.providers.TongueTwistersProvider.TongueTwisters.COLUMN_TONGUE_TWISTER;
import static app.providers.TongueTwistersProvider.TongueTwisters.COLUMN_TYPE;
import static app.providers.TongueTwistersProvider.TongueTwisters.TYPE_USER_DEFINED;

/**
 * Tongue twister creator fragment.
 */
public class TongueTwisterCreatorFragment extends BaseFragment {

    private EditText editTongueTwister, editFilename;
    private View cmdViewSave;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment__tongue_twister_creator, container, false);

        // MAP CONTROLS

        editTongueTwister = Views.findById(rootView, R.id.edit__tongue_twister);
        editFilename = Views.findById(rootView, R.id.edit__filename);
        cmdViewSave = rootView.findViewById(R.id.text__save);

        return rootView;
    }//onCreateView()

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // SETUP CONTROLS

        editTongueTwister.removeTextChangedListener(editTongueTwisterTextWatcher);
        editTongueTwister.addTextChangedListener(editTongueTwisterTextWatcher);

        for (final TextView v : new TextView[]{editFilename, editTongueTwister})
            v.setTypeface(Resources.getTypefaceGoboldRegular(getContext()));

        cmdViewSave.setOnClickListener(commandViewsOnClickListener);
        view.findViewById(R.id.view_group__tongue_twister).setOnClickListener(commandViewsOnClickListener);
    }//onViewCreated()

    @Override
    public void onStart() {
        super.onStart();

        editFilename.post(new Runnable() {

            @Override
            public void run() {
                if (editFilename.requestFocus()) UI.showSoftInput(getContext(), editFilename, true);
            }//run()

        });
    }//onStart()

    /**
     * Text watcher for edit tongue twister.
     */
    private final TextWatcher editTongueTwisterTextWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }//beforeTextChanged()

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }//onTextChanged()

        @Override
        public void afterTextChanged(Editable s) {
            cmdViewSave.setEnabled(s.toString().trim().length() > 0);
        }//afterTextChanged()

    };//editTongueTwisterTextWatcher

    /**
     * On click listener for all command views.
     */
    private final View.OnClickListener commandViewsOnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.text__save: {
                final String tongueTwister = editTongueTwister.getText().toString().trim();
                if (TextUtils.isEmpty(tongueTwister)) {
                    editTongueTwister.setText(null);
                    break;
                }//if

                final String title = editFilename.getText().toString().trim();

                final ContentValues values = new ContentValues();
                values.put(COLUMN_TYPE, TYPE_USER_DEFINED);
                values.put(COLUMN_TONGUE_TWISTER, tongueTwister);
                values.put(COLUMN_TITLE, title);

                final Uri contentUri = SimpleContract.getContentUri(getContext(), TongueTwistersProvider.class, TongueTwisters.class);
                CPOExecutor.IntentBuilder.newBulkInsert(getContext(), null, contentUri).addValues(values).start();

                finishActivity();

                break;
            }//SAVE

            case R.id.view_group__tongue_twister: {
                if (editTongueTwister.requestFocus()) UI.showSoftInput(getContext(), editTongueTwister, true);
                break;
            }//view_group__tongue_twister
            }
        }//onClick()

    };//commandViewsOnClickListener

}