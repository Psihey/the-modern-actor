package app.fragments.tongueTwister;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.AmateurFragmentStatePagerAdapter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.R;
import app.fragments.tongueTwister.recording.FragmentTongueTwisterRecording;
import app.providers.TongueTwistersProvider;
import app.providers.TongueTwistersProvider.TongueTwisters;
import haibison.android.fad7.Fad7;
import haibison.android.res.Ids;
import haibison.android.simpleprovider.SimpleContract;
import haibison.android.underdogs.Param;

import static app.providers.TongueTwistersProvider.TongueTwisters.COLUMN_TONGUE_TWISTER;
import static haibison.android.simpleprovider.database.BaseTable._ID;

/**
 * Horizontal Tongue Twisters fragment.
 */
public class HorizontalTongueTwistersFragment extends Fad7 implements LoaderCallbacks<Cursor> {

    private static final String CLASSNAME = HorizontalTongueTwistersFragment.class.getName();

    @Param(type = Param.Type.INPUT, dataTypes = long.class)
    public static final String EXTRA_SELECTED_ID = CLASSNAME + ".SELECTED_ID";

    private final int loaderIdData = Ids.newUid();

    private ViewPager pager;
    private TongueTwistersAdapter tongueTwistersAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment__horizontal_tongue_twisters, container, false);

        pager = (ViewPager) rootView.findViewById(R.id.pager);

        return rootView;
    }//onCreateView()


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        pager.addOnPageChangeListener(pagerOnPageChangeListener);
           setHasOptionsMenu(true);
        tongueTwistersAdapter = new TongueTwistersAdapter(getChildFragmentManager());
        pager.setAdapter(tongueTwistersAdapter);
    }//onViewCreated()

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(loaderIdData, null, this);
    }// onActivityCreated()

    /*****************
     * LoaderCallbacks
     *****************/

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == loaderIdData) {
            return new CursorLoader(
                    getActivity(), SimpleContract.getContentUri(getContext(), TongueTwistersProvider.class, TongueTwisters.class),
                    null, null, null, null
            );
        }// loaderIdData()

        return null;
    }// onCreateLoader()


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (loader.getId() == loaderIdData) {
            tongueTwistersAdapter.changeCursor(cursor);
//            // Update activity title
//            if (tongueTwistersAdapter.getCount() > 0 && pager.getCurrentItem() == 0) pagerOnPageChangeListener.onPageSelected(0);

            if (cursor != null && cursor.moveToFirst() && getArguments().containsKey(EXTRA_SELECTED_ID)) {
                final long selectedId = getArguments().getLong(EXTRA_SELECTED_ID);
                getArguments().remove(EXTRA_SELECTED_ID);

                do {
                    final long id = cursor.getLong(cursor.getColumnIndex(_ID));
                    if (id == selectedId) {
                        final int position = cursor.getPosition();
                        pager.post(new Runnable() {

                            @Override
                            public void run() {
                                pager.setCurrentItem(position);
                            }//run()

                        });

                        break;
                    }//if
                } while (cursor.moveToNext());
            }//if
        }// loaderIdData
    }// onLoadFinished()

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (loader.getId() == loaderIdData) {
            tongueTwistersAdapter.changeCursor(null);
        }// loaderIdData
    }// onLoaderReset()

    /**
     * Adapter of Tongue Twisters.
     */
    private class TongueTwistersAdapter extends AmateurFragmentStatePagerAdapter {

        private Cursor cursor;
        private int colId, colTongueTwister;

        /**
         * Makes new instance.
         *
         * @param fm fragment manager.
         */
        public TongueTwistersAdapter(FragmentManager fm) {
            super(fm);
        }//TongueTwistersAdapter()

        /**
         * Changes cursor.
         *
         * @param newCursor new cursor.
         */
        public synchronized void changeCursor(Cursor newCursor) {
            if (cursor != null) cursor.close();
            cursor = newCursor;

            if (cursor != null) {
                colId = cursor.getColumnIndex(_ID);
                colTongueTwister = cursor.getColumnIndex(COLUMN_TONGUE_TWISTER);
            }//if

            notifyDataSetChanged();
        }//changeCursor()

        @Override
        public int getCount() {
            return cursor != null ? cursor.getCount() : 0;
        }//getCount()

        @Override
        public Fragment getItem(int position) {
            cursor.moveToPosition(position);
            final long id = cursor.getLong(colId);

            final FragmentTongueTwisterRecording fragment = new FragmentTongueTwisterRecording();
            fragment.getArguments().putLong(FragmentTongueTwisterRecording.EXTRA_TONGUE_TWISTER_ID, id);
            fragment.getArguments().putBoolean(FragmentTongueTwisterRecording.EXTRA_UPDATE_ACTIVITY_TITLE, false);

            return fragment;
        }//getItem()

        @Override
        public CharSequence getPageTitle(int position) {
            cursor.moveToPosition(position);
            return cursor.getString(colTongueTwister);
        }//getPageTitle()

    }//TongueTwistersAdapter

    /**
     * on Page Change Listener for pager.
     */
//    private final ViewPager.OnPageChangeListener pagerOnPageChangeListener = new AmateurViewPager.SimpleOnPageChangeListener() {
//
//        @Override
//        public void onPageSelected(int position) {
//            super.onPageSelected(position);
//            getActivity().setTitle("");
//        }//onPageSelected()
//
//    };//pagerOnPageChangeListener

}