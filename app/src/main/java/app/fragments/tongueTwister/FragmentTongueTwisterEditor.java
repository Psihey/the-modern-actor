package app.fragments.tongueTwister;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import app.R;
import app.fragments.base.BaseFragment;
import app.providers.TongueTwistersProvider;
import app.providers.TongueTwistersProvider.TongueTwisters;
import haibison.android.fad7.utils.Views;
import haibison.android.res.Ids;
import haibison.android.simpleprovider.SimpleContract;
import haibison.android.wls.ToastsService;

import static app.providers.TongueTwistersProvider.TongueTwisters.COLUMN_TONGUE_TWISTER;

/**
 * Fragment Tongue Twister editor.
 */
public class FragmentTongueTwisterEditor extends BaseFragment implements LoaderCallbacks<Cursor> {

    private static final String CLASSNAME = FragmentTongueTwisterEditor.class.getName();

    /**
     * ID of tongue twister.
     * <p/>
     * Type: Long, optional.
     */
    public static final String EXTRA_TONGUE_TWISTER_ID = CLASSNAME + ".TONGUE_TWISTER_ID";

    private final int loaderIdData = Ids.newUid();

    private View contentBodyView, progressBar;
    private EditText editTongueTwister;
    private Cursor cursorData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View contentView = inflater.inflate(R.layout.fragment__tongue_twister_editor, container, false);

        // MAP CONTROLS

        contentBodyView = Views.findById(contentView, R.id.content_body);
        editTongueTwister = Views.findById(contentView, R.id.edit__tongue_twister);
        progressBar = Views.findById(contentView, R.id.progress_bar);

        // SETUP CONTROLS

        final int height = editTongueTwister.getBackground().getIntrinsicHeight();
        editTongueTwister.getLayoutParams().height = height;
        editTongueTwister.requestLayout();

        for (final int id : new int[]{R.id.img__save})
            contentView.findViewById(id).setOnClickListener(mButtonsOnClickListener);

        return contentView;
    }// onCreateView()

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments().containsKey(EXTRA_TONGUE_TWISTER_ID))
            getLoaderManager().initLoader(loaderIdData, null, this);
        else
            showProgressBar(false);
    }// onActivityCreated()

    @Override
    public void onDestroy() {
        closeAllCursors();

        super.onDestroy();
    }// onDestroy()

    //////////////////
    // LoaderCallbacks
    //////////////////

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == loaderIdData) {
            showProgressBar(true);

            return new CursorLoader(getActivity(),
                    SimpleContract.getContentItemUri(getContext(),
                            TongueTwistersProvider.class, TongueTwisters.class,
                            getArguments().getLong(EXTRA_TONGUE_TWISTER_ID)),
                    null, null, null, null);

        }// loaderIdData

        return null;
    }// onCreateLoader()

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == loaderIdData) {
            closeAllCursors();

            cursorData = data;
            if (cursorData != null && cursorData.moveToFirst()) {
                editTongueTwister.setText(cursorData.getString(cursorData
                        .getColumnIndex(COLUMN_TONGUE_TWISTER)));
            }// if

            showProgressBar(false);
        }// loaderIdData
    }// onLoadFinished()

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (loader.getId() == loaderIdData) {
            closeAllCursors();
        }// loaderIdData
    }// onLoaderReset()

    /**
     * Shows or hides progress bar.
     *
     * @param shown {@code true} or {@code false}.
     */
    private void showProgressBar(boolean shown) {
        if (shown) {
            contentBodyView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
            contentBodyView.setVisibility(View.VISIBLE);
        }
    }// showProgressBar()

    /**
     * Closes all cursors.
     */
    private synchronized void closeAllCursors() {
        if (cursorData != null) {
            cursorData.close();
            cursorData = null;
        }// if
    }// closeAllCursors()

    /***********
     * LISTENERS
     ***********/

    /**
     * Click listener for all buttons.
     */
    private final View.OnClickListener mButtonsOnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.img__save: {
                ContentValues values = new ContentValues();
                values.put(COLUMN_TONGUE_TWISTER, editTongueTwister.getText()
                        .toString().trim());

                boolean ok = false;

                if (getArguments().containsKey(EXTRA_TONGUE_TWISTER_ID)) {
                    ok = getActivity().getContentResolver().update(
                            SimpleContract.getContentItemUri(
                                    getContext(), TongueTwistersProvider.class, TongueTwisters.class,
                                    getArguments().getLong(EXTRA_TONGUE_TWISTER_ID)),
                            values, null, null
                    ) > 0;
                } else {
                    final Uri uri = getActivity().getContentResolver().insert(
                            SimpleContract.getContentUri(getContext(), TongueTwistersProvider.class, TongueTwisters.class), values
                    );
                    if (uri != null) {
                        // Tongue Twister provider made sure we got a long value at the end of the URI.
                        getArguments().putLong(EXTRA_TONGUE_TWISTER_ID, Long.parseLong(uri.getLastPathSegment()));
                        ok = true;
                    }// if
                }

                if (ok) ToastsService.toastShort(getActivity(), R.string.saved);

                break;
            }// SAVE
            }
        }// onClick()

    };// mButtonsOnClickListener

}