package app.fragments.actorVerbs;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import app.R;
import app.adapters.AdapterWords;
import app.fragments.base.RecyclerFragment;
import app.providers.WordsProvider;
import app.providers.WordsProvider.MainWords;
import app.providers.WordsProvider.SimilarWords;
import app.utils.Resources;
import app.utils.UI;

import haibison.android.fad7.utils.Views;
import haibison.android.res.Ids;
import haibison.android.simpleprovider.SimpleContract;

/**
 * Fragment Words.
 */
public class FragmentWords extends RecyclerFragment implements LoaderCallbacks<Cursor> {

    private static final String CLASSNAME = FragmentWords.class.getName();

    /**
     * Use this extra to filter main words.
     * <p/>
     * Type: {@link CharSequence}, optional.
     */
    private static final String EXTRA_MAIN_WORD_QUERY = CLASSNAME + ".MAIN_WORD_QUERY";

    /**
     * Use this extra to query similar words.
     * <p/>
     * Type: Long, required.
     */
    private static final String EXTRA_MAIN_WORD_ID = CLASSNAME + ".MAIN_WORD_ID";

    private final int loaderIdMainWords = Ids.newUid();
    private final int loaderIdSimilarWords = Ids.newUid();

    private EditText editSearch;
    private AdapterWords adapterWords;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // MAP CONTROLS

        editSearch = Views.findById(view, R.id.edit__search);

        // SETUP CONTROLS

        editSearch.setTypeface(Resources.getTypefaceGoboldRegular(getContext()));
        editSearch.addTextChangedListener(editSearchTextWatcher);
        editSearch.setOnEditorActionListener(editSearchOnEditorActionListener);

        for (final int id : new int[]{R.id.text__search, R.id.content})
            view.findViewById(id).setOnClickListener(mButtonsOnClickListener);
    }// onViewCreated()

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setEmptyText(R.string.text__no_data);

        adapterWords = new AdapterWords();
        adapterWords.setEventListener(adapterWordsEventListener);
        setAdapter(adapterWords);

        hideProgressBar();

        getLoaderManager().initLoader(loaderIdMainWords, new Bundle(), this);
    }// onActivityCreated()

    @Override
    public void onDestroy() {
        adapterWords.changeCursor(null, false);

        super.onDestroy();
    }// onDestroy()

    ///////////////////
    // RecyclerFragment
    ///////////////////

    @Override
    protected int getResourceLayoutId() {
        return R.layout.fragment__words;
    }// getResourceLayoutId()

    @Override
    protected LayoutManager newLayoutManager(Context context) {
        return new LinearLayoutManager(context);
    }// newLayoutManager()

    //////////////////
    // LoaderCallbacks
    //////////////////

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == loaderIdMainWords) {
            showProgressBar();

            CharSequence query = args.getCharSequence(EXTRA_MAIN_WORD_QUERY);
            if (TextUtils.isEmpty(query) == false) {
                StringBuilder tmp = new StringBuilder(DatabaseUtils.sqlEscapeString(String.valueOf(query)));
                tmp.insert(tmp.length() - 1, "%");

                query = MainWords.COLUMN_WORD + " LIKE " + tmp;
            }// if

            return new CursorLoader(
                    getActivity(), SimpleContract.getContentUri(getContext(), WordsProvider.class, MainWords.class),
                    null, TextUtils.isEmpty(query) ? null : query.toString(), null, null
            );
        }// loaderIdMainWords
        else if (id == loaderIdSimilarWords) {
            showProgressBar();

            String selection = SimilarWords.COLUMN_MAIN_WORD_ID + "=" + args.getLong(EXTRA_MAIN_WORD_ID);

            return new CursorLoader(
                    getActivity(), SimpleContract.getContentUri(getContext(), WordsProvider.class, SimilarWords.class),
                    null, selection, null, null
            );
        }// loaderIdSimilarWords()

        return null;
    }// onCreateLoader()

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (loader.getId() == loaderIdMainWords) {
            adapterWords.changeCursor(cursor, true);

            if (cursor != null && cursor.getCount() > 0) UI.showSoftInput(getActivity(), editSearch, false);

            hideProgressBar();
        }// loaderIdMainWords
        else if (loader.getId() == loaderIdSimilarWords) {
            adapterWords.changeCursor(cursor, false);

            if (cursor != null && cursor.getCount() > 0) UI.showSoftInput(getActivity(), editSearch, false);

            hideProgressBar();
        }// loaderIdSimilarWords
    }// onLoadFinished()

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (loader.getId() == loaderIdMainWords) {
            adapterWords.changeCursor(null, true);
        }// loaderIdMainWords
        else if (loader.getId() == loaderIdSimilarWords) {
            adapterWords.changeCursor(null, false);
        }// loaderIdSimilarWords
    }// onLoaderReset()

    /**
     * Updates list of main words.
     *
     * @param query the query. If {@code null} or empty, all main words will be
     *              shown.
     */
    private void updateListOfMainWords(CharSequence query) {
        final Bundle args = new Bundle();
        args.putCharSequence(EXTRA_MAIN_WORD_QUERY, query);
        getLoaderManager().restartLoader(loaderIdMainWords, args, this);
    }// updateListOfMainWords()

    /***********
     * LISTENERS
     ***********/

    /**
     * Text watcher for edit search.
     */
    private final TextWatcher editSearchTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // Do nothing
        }// onTextChanged()

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // Do nothing
        }// beforeTextChanged()

        @Override
        public void afterTextChanged(Editable s) {
            if (TextUtils.isEmpty(s)) updateListOfMainWords(s);
        }// afterTextChanged()

    };// editSearchTextWatcher

    /**
     * Editor action listener for edit search.
     */
    private final TextView.OnEditorActionListener editSearchOnEditorActionListener = new TextView.OnEditorActionListener() {

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            switch (actionId) {
            case EditorInfo.IME_ACTION_SEARCH: {
                updateListOfMainWords(v.getText());
                return true;
            }// IME_ACTION_SEARCH
            }

            return false;
        }// onEditorAction()

    };// editSearchOnEditorActionListener

    /**
     * Click listener for all buttons.
     */
    private final View.OnClickListener mButtonsOnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.text__search: {
                updateListOfMainWords(editSearch.getText());
                break;
            }// SEARCH

            case R.id.content: {
                if (adapterWords == null || adapterWords.getItemCount() == 0) {
                    // Show all main words
                    updateListOfMainWords(null);
                }// if

                break;
            }// CONTENT
            }
        }// onClick()

    };// mButtonsOnClickListener

    /**
     * Event listener for adapter Similar Words.
     */
    private final AdapterWords.EventListener adapterWordsEventListener = new AdapterWords.EventListener() {

        @Override
        public void onItemClick(long wordId, boolean isMainWord) {
            if (isMainWord) {
                // Load similar words
                final Bundle args = new Bundle();
                args.putLong(EXTRA_MAIN_WORD_ID, wordId);
                getLoaderManager().restartLoader(loaderIdSimilarWords, args, FragmentWords.this);
            } else {
                // Do nothing
            }
        }// onItemClick()

    };// adapterWordsEventListener

}