//package app.fragments.singing;
//
//import android.os.Build;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import app.R;
//import app.fragments.warmUp.BaseMediaPlayerFragment;
//import app.widgets.MediaController;
//
//import haibison.android.fad7.utils.Views;
//
///**
// * Fragment Singing and Acting Vocal Warmup.
// */
//public class FragmentSingingAndActingVocalWarmup extends BaseMediaPlayerFragment {
//
//    private MediaController mediaController;
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        final View contentView = inflater.inflate(R.layout.fragment__singing_and_acting_vocal_warmup, container, false);
//
//        // MAP CONTROLS
//
//        mediaController = Views.findById(contentView, R.id.media_controller);
//
//        // SETUP CONTROLS
//
//        for (final int id : new int[] { R.id.img__singing_warmup, R.id.img__acting_vocal_warmup })
//            contentView.findViewById(id).setOnClickListener(mButtonsOnClickListener);
//
//        return contentView;
//    }// onCreateView()
//
//    ////////////
//    // LISTENERS
//    ////////////
//
//    /**
//     * Click listener for all buttons.
//     */
//    private final View.OnClickListener mButtonsOnClickListener = new View.OnClickListener() {
//
//        @Override
//        public void onClick(View v) {
//            final ViewGroup parentView = (ViewGroup) mediaController.getParent();
//
//            switch (v.getId()) {
//            case R.id.img__singing_warmup: {
//                if (parentView.indexOfChild(mediaController) != 0) {
//                    parentView.removeView(mediaController);
//                    parentView.addView(mediaController, 0, mediaController.getLayoutParams());
//
//                    mediaController.setMediaPlayer(newMediaPlayer(R.raw.mp3__singing_warmup));
//                    mediaController.setVisibility(View.VISIBLE);
//                }// if
//
//                break;
//            }// SINGING WARMUP
//
//            case R.id.img__acting_vocal_warmup: {
//                if (parentView.indexOfChild(mediaController) != parentView.getChildCount() - 1) {
//                    parentView.bringChildToFront(mediaController);
//                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
//                        parentView.requestLayout();
//                        parentView.invalidate();
//                    }// if
//
//                    mediaController.setMediaPlayer(newMediaPlayer(R.raw.mp3__the_actor_warmup));
//                    mediaController.setVisibility(View.VISIBLE);
//                }// if
//
//                break;
//            }// ACTING VOCAL WARMUP
//            }
//        }// onClick()
//
//    };// mButtonsOnClickListener
//
//}