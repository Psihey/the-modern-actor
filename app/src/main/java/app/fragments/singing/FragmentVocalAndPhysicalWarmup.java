//package app.fragments.singing;
//
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import app.activities.BaseAdsActivity;
//import app.R;
//import app.fragments.warmUp.BaseMediaPlayerFragment;
//import app.widgets.MediaController;
//import haibison.android.fad7.utils.Views;
//
///**
// * Fragment Vocal and Physical Warmup.
// */
//public class FragmentVocalAndPhysicalWarmup extends BaseMediaPlayerFragment {
//
//    private MediaController mediaController;
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        final View contentView = inflater.inflate(R.layout.fragment__vocal_and_physical_warmup, container, false);
//
//        // MAP CONTROLS
//
//        mediaController = Views.findById(contentView, R.id.media_controller);
//
//        // SETUP CONTROLS
//
//        for (final int id : new int[]{R.id.img__physical_warmup, R.id.img__singing_warmup})
//            contentView.findViewById(id).setOnClickListener(mButtonsOnClickListener);
//
//        return contentView;
//    }// onCreateView()
//
//    ////////////
//    // LISTENERS
//    ////////////
//
//    /**
//     * Click listener for all buttons.
//     */
//    private final View.OnClickListener mButtonsOnClickListener = new View.OnClickListener() {
//
//        @Override
//        public void onClick(View v) {
//            switch (v.getId()) {
//            case R.id.img__physical_warmup: {
//                if (getMediaPlayer() == null) {
//                    mediaController.setMediaPlayer(newMediaPlayer(R.raw.mp3__physical_warmup));
//                    mediaController.setVisibility(View.VISIBLE);
//                }// if
//
//                break;
//            }// PHYSICAL WARMUP
//
//            case R.id.img__singing_warmup: {
//                releaseMediaPlayer();
//                mediaController.setVisibility(View.GONE);
//
//                BaseAdsActivity.newIntentBuilder(getContext()).setFragment(FragmentSingingAndActingVocalWarmup.class).start();
//
//                break;
//            }// SINGING WARMUP
//            }
//        }// onClick()
//
//    };// mButtonsOnClickListener
//
//}