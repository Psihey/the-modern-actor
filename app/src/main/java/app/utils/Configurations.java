package app.utils;

/**
 * Configurations.
 */
public class Configurations {

    // Singleton class
    private Configurations() {}

    /**
     * I/O buffer size, in bytes.
     */
    public static final int IO_BUFFER = 1024 * 8;

}