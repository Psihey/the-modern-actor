package app.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.ArrayRes;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;

import app.R;

/**
 * Action.
 */
public class Action implements Parcelable {

    /**
     * Parses action from given style.
     *
     * @param context the context.
     * @param style   style resource ID.
     * @return the action.
     */
    @NonNull
    public static Action parse(@NonNull Context context, @StyleRes int style) {
        final TypedArray a = context.obtainStyledAttributes(style, R.styleable.Action);
        try {
            return new Action(a.getResourceId(R.styleable.Action_android_id, 0), a.getString(R.styleable.Action_android_title));
        } finally {
            a.recycle();
        }
    }//parse()

    /**
     * Parse actions from given array.
     *
     * @param context the context.
     * @param array   array resource ID.
     * @return the actions.
     */
    @NonNull
    public static Action[] parseActions(@NonNull Context context, @ArrayRes int array) {
        final TypedArray a = context.getResources().obtainTypedArray(array);
        try {
            final Action[] actions = new Action[a.length()];
            for (int i = 0; i < a.length(); i++) actions[i] = parse(context, a.getResourceId(i, 0));
            return actions;
        } finally {
            a.recycle();
        }
    }//parseActions()

    /**
     * Parse array of arrays of actions from given array.
     *
     * @param context the context.
     * @param array   array resource ID.
     * @return the array of arrays of actions.
     */
    @NonNull
    public static Action[][] parseArraysOfActions(@NonNull Context context, @ArrayRes int array) {
        final TypedArray a = context.getResources().obtainTypedArray(array);
        try {
            final Action[][] arraysOfActions = new Action[a.length()][];
            for (int i = 0; i < a.length(); i++) arraysOfActions[i] = parseActions(context, a.getResourceId(i, 0));
            return arraysOfActions;
        } finally {
            a.recycle();
        }
    }//parseArraysOfActions()

    /**
     * ID.
     */
    @IdRes
    public final int id;

    /**
     * Title.
     */
    @Nullable
    public final String title;

    /**
     * Makes new instance.
     *
     * @param id    see {@link #id}.
     * @param title see {@link #title}.
     */
    private Action(@IdRes int id, @Nullable String title) {
        this.id = id;
        this.title = title;
    }//Action()

    /////////////
    // Parcelable
    /////////////

    @Override
    public int describeContents() {
        return 0;
    }//describeContents()

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
    }//writeToParcel()

    // Required by Parcelable
    public static final Parcelable.Creator<Action> CREATOR = new Parcelable.Creator<Action>() {

        @Override
        public Action createFromParcel(Parcel source) {
            return new Action(source.readInt(), source.readString());
        }//createFromParcel()

        @Override
        public Action[] newArray(int size) {
            return new Action[size];
        }//newArray()

    };//CREATOR

}