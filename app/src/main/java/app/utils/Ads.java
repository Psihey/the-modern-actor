package app.utils;

import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdRequest.Builder;

/**
 * Ads utilities.
 */
public final class Ads {

    // Singleton class
    private Ads() {}

    /**
     * AdMob.
     */
    public static final class AdMob {

        // Singleton class
        private AdMob() {}

        /**
         * Test device IDs.
         */
        public static final String[] TEST_DEVICE_IDS = {
                // Hai Le (Sony Xperia M2)
                "784D1D621525543268065F3009C2741A",
                // Hai Le (Sony Xperia M4)
                "1CA34E41D21B5418CF8CB26132818310"
        };

        /**
         * Banner ad unit ID.
         */
        public static final String BANNER_AD_UNIT_ID = "ca-app-pub-0000000000000000/0000000000";// TODO

        /**
         * Interstitial ad unit ID.
         */
        public static final String INTERSTITIAL_AD_UNIT_ID = "ca-app-pub-0000000000000000/0000000000";// TODO

        /**
         * Checks if ads is supported on this version of Android.
         *
         * @return {@code true} or {@code false}.
         */
        public static boolean isSupported() {
            // Per requirement: ads are disabled
            if (true) return false;
            return VERSION.SDK_INT >= VERSION_CODES.GINGERBREAD;
        }//isSupported()

        /**
         * Creates new ad-request.
         *
         * @return new ad-request.
         */
        public static AdRequest newAdRequest() {
            final Builder builder = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
            for (final String id : TEST_DEVICE_IDS) builder.addTestDevice(id);
            return builder.build();
        }//newAdRequest()

    }//AdMob

}