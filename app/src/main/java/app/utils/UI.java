package app.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * UI utilities.
 */
public class UI {

    // Singleton class
    private UI() {}

    /**
     * Shows or hides soft input.
     *
     * @param context the context.
     * @param view    target view. If {@code null}, this method does nothing.
     * @param shown   {@code true} or {@code false}.
     */
    public static void showSoftInput(@NonNull Context context, @NonNull final View view, boolean shown) {
        final InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (shown) view.post(new Runnable() {

            @Override
            public void run() {
                imm.showSoftInput(view, 0, null);
            }// run()

        }); else imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }// showSoftInput()

}