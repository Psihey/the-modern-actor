package app.utils;

import android.content.Context;
import android.graphics.Typeface;

import java.io.File;

import static app.BuildConfig.APPLICATION_ID;

/**
 * Resource utilities.
 */
public class Resources {

    // Singleton class
    private Resources() {}

    /**
     * Asset path root.
     */
    public static final String ASSET_PATH_ROOT = APPLICATION_ID.replace('.', File.separatorChar);

    /**
     * Asset path of folder of fonts.
     */
    public static final String ASSET_PATH_FONTS = joinPaths(ASSET_PATH_ROOT, "fonts");

    /**
     * Asset path of font "Gobold-Regular.ttf".
     */
    public static final String ASSET_PATH_FONT_GOBOLD_REGULAR = joinPaths(ASSET_PATH_FONTS, "Gobold-Regular.ttf");

    /**
     * Joins paths with {@link File#separatorChar}.
     *
     * @param paths paths.
     * @return the final path.
     */
    public static final String joinPaths(String... paths) {
        final StringBuilder sb = new StringBuilder();
        for (final String path : paths) {
            if (sb.length() > 0) sb.append(File.separatorChar);
            sb.append(path);
        }//for

        return sb.toString();
    }//joinPaths()

    private static Typeface mTypefaceGoboldRegular;

    /**
     * Gets typeface Gobold Regular.
     *
     * @param context the context.
     * @return typeface Gobold Regular.
     */
    public static synchronized Typeface getTypefaceGoboldRegular(Context context) {
        if (mTypefaceGoboldRegular == null)
            mTypefaceGoboldRegular = Typeface.createFromAsset(context.getAssets(), ASSET_PATH_FONT_GOBOLD_REGULAR);
        return mTypefaceGoboldRegular;
    }//getTypefaceGoboldRegular()

}