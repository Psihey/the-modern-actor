package app.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import app.R;

/**
 * Common intents and utilities.
 */
public class Intents {

    // Singleton class
    private Intents() {}

    /**
     * MIME: any audio.
     */
    public static final String MIME_ANY_AUDIO = "audio/*";

    /**
     * MIME type of .mp4 files.
     */
    public static final String MIME_AUDIO_MP4 = "audio/mp4";

    /**
     * Creates an intent to open a URL. You must call this intent from a UI
     * thread.
     *
     * @param url the URL.
     * @return the intent.
     */
    public static Intent newIntentToOpenUrl(String url) {
        return new Intent(Intent.ACTION_VIEW, Uri.parse(url));
    }// newIntentToOpenUrl()

    /**
     * Sends an intent to open a URL. If no apps found to handle URLs, shows the
     * user a message about that.
     * <p/>
     * You must call this method from a UI thread.
     *
     * @param context the context.
     * @param url     the URL.
     */
    public static void sendIntentToOpenUrl(Context context, String url) {
        context.startActivity(Intent.createChooser(newIntentToOpenUrl(url), context.getString(R.string.msg__open_url_with)));
    }// sendIntentToOpenUrl()

}