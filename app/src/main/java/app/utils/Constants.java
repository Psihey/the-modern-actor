package app.utils;

import static app.BuildConfig.APPLICATION_ID;

/**
 * Constants.
 */
public final class Constants {

    // Singleton class
    private Constants() {}

    /**
     * URL of home page.
     */
    public static final String URL_HOMEPAGE = "http://themodernactor.com";

    /**
     * URL of app page on Google Play Store.
     */
    public static final String URL_GOOGLE_PLAY_STORE_APP_PAGE = "https://play.google.com/store/apps/details?id=" + APPLICATION_ID;

    /**
     * URL of developer page on Google Play Store.
     */
    public static final String URL_GOOGLE_PLAY_STORE_DEV_PAGE = "https://play.google.com/store/apps/dev?id=03918263470804342850";

}